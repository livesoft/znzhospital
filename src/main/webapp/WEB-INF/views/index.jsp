<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8" />
    <title>首页</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/jquery.gritter.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/daterangepicker.css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/fullcalendar.css" />

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/jqvmap.css" rel="stylesheet" type="text/css"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>

    <!-- END PAGE LEVEL STYLES -->

</head>
<body>

<div id="dashboard">

<!-- BEGIN DASHBOARD STATS -->

<div class="row-fluid">

    <div class="span3 responsive" data-tablet="span6" data-desktop="span3">

        <div class="dashboard-stat blue">

            <div class="visual">

                <i class="icon-comments"></i>

            </div>

            <div class="details">

                <div class="number">

                    10

                </div>

                <div class="desc">

                    分配给我的需求数

                </div>

            </div>

            <a class="more" href="#">

                查看<i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

    </div>

    <div class="span3 responsive" data-tablet="span6" data-desktop="span3">

        <div class="dashboard-stat green">

            <div class="visual">

                <i class="icon-shopping-cart"></i>

            </div>

            <div class="details">

                <div class="number">549</div>

                <div class="desc">New Orders</div>

            </div>

            <a class="more" href="#">

                View more <i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

    </div>

    <div class="span3 responsive" data-tablet="span6  fix-offset" data-desktop="span3">

        <div class="dashboard-stat purple">

            <div class="visual">

                <i class="icon-globe"></i>

            </div>

            <div class="details">

                <div class="number">+89%</div>

                <div class="desc">Brand Popularity</div>

            </div>

            <a class="more" href="#">

                View more <i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

    </div>

    <div class="span3 responsive" data-tablet="span6" data-desktop="span3">

        <div class="dashboard-stat yellow">

            <div class="visual">

                <i class="icon-bar-chart"></i>

            </div>

            <div class="details">

                <div class="number">210,000￥</div>

                <div class="desc">本月业绩总额</div>

            </div>

            <a class="more" href="#">

                查看<i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

    </div>

</div>

<!-- END DASHBOARD STATS -->

<div class="clearfix"></div>

<div class="row-fluid">

    <div class="span6">

        <div class="portlet box purple">

            <div class="portlet-title">

                <div class="caption"><i class="icon-calendar"></i>General Stats</div>

                <div class="actions">

                    <a href="javascript:;" class="btn yellow easy-pie-chart-reload"><i class="icon-repeat"></i> Reload</a>

                </div>

            </div>

            <div class="portlet-body">

                <div class="row-fluid">

                    <div class="span4">

                        <div class="easy-pie-chart">

                            <div class="number transactions"  data-percent="55"><span>+55</span>%</div>

                            <a class="title" href="#">Transactions <i class="m-icon-swapright"></i></a>

                        </div>

                    </div>

                    <div class="margin-bottom-10 visible-phone"></div>

                    <div class="span4">

                        <div class="easy-pie-chart">

                            <div class="number visits"  data-percent="85"><span>+85</span>%</div>

                            <a class="title" href="#">New Visits <i class="m-icon-swapright"></i></a>

                        </div>

                    </div>

                    <div class="margin-bottom-10 visible-phone"></div>

                    <div class="span4">

                        <div class="easy-pie-chart">

                            <div class="number bounce"  data-percent="46"><span>-46</span>%</div>

                            <a class="title" href="#">Bounce <i class="m-icon-swapright"></i></a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="span6">

        <div class="portlet box blue">

            <div class="portlet-title">

                <div class="caption"><i class="icon-calendar"></i>Server Stats</div>

                <div class="tools">

                    <a href="" class="collapse"></a>

                    <a href="#portlet-config" data-toggle="modal" class="config"></a>

                    <a href="" class="reload"></a>

                    <a href="" class="remove"></a>

                </div>

            </div>

            <div class="portlet-body">

                <div class="row-fluid">

                    <div class="span4">

                        <div class="sparkline-chart">

                            <div class="number" id="sparkline_bar"></div>

                            <a class="title" href="#">Network <i class="m-icon-swapright"></i></a>

                        </div>

                    </div>

                    <div class="margin-bottom-10 visible-phone"></div>

                    <div class="span4">

                        <div class="sparkline-chart">

                            <div class="number" id="sparkline_bar2"></div>

                            <a class="title" href="#">CPU Load <i class="m-icon-swapright"></i></a>

                        </div>

                    </div>

                    <div class="margin-bottom-10 visible-phone"></div>

                    <div class="span4">

                        <div class="sparkline-chart">

                            <div class="number" id="sparkline_line"></div>

                            <a class="title" href="#">Load Rate <i class="m-icon-swapright"></i></a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="clearfix"></div>

<div class="row-fluid">

<div class="span6">

    <!-- BEGIN PORTLET-->

    <div class="portlet box blue calendar">

        <div class="portlet-title">

            <div class="caption"><i class="icon-calendar"></i>Calendar</div>

        </div>

        <div class="portlet-body light-grey">

            <div id="calendar">

            </div>

        </div>

    </div>

    <!-- END PORTLET-->

</div>

<div class="span6">

<!-- BEGIN PORTLET-->

<div class="portlet">

<div class="portlet-title line">

    <div class="caption"><i class="icon-comments"></i>Chats</div>

    <div class="tools">

        <a href="" class="collapse"></a>

        <a href="#portlet-config" data-toggle="modal" class="config"></a>

        <a href="" class="reload"></a>

        <a href="" class="remove"></a>

    </div>

</div>

<div class="portlet-body" id="chats">

<div class="scroller" data-height="435px" data-always-visible="1" data-rail-visible1="1">

    <ul class="chats">

        <li class="in">

            <img class="avatar" alt="" src="media/image/avatar1.jpg" />

            <div class="message">

                <span class="arrow"></span>

                <a href="#" class="name">Bob Nilson</a>

                <span class="datetime">at Jul 25, 2012 11:09</span>

													<span class="body">

													Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

													</span>

            </div>

        </li>

        <li class="out">

            <img class="avatar" alt="" src="media/image/avatar2.jpg" />

            <div class="message">

                <span class="arrow"></span>

                <a href="#" class="name">Lisa Wong</a>

                <span class="datetime">at Jul 25, 2012 11:09</span>

													<span class="body">

													Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

													</span>

            </div>

        </li>

        <li class="in">

            <img class="avatar" alt="" src="media/image/avatar1.jpg" />

            <div class="message">

                <span class="arrow"></span>

                <a href="#" class="name">Bob Nilson</a>

                <span class="datetime">at Jul 25, 2012 11:09</span>

													<span class="body">

													Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

													</span>

            </div>

        </li>

        <li class="out">

            <img class="avatar" alt="" src="media/image/avatar3.jpg" />

            <div class="message">

                <span class="arrow"></span>

                <a href="#" class="name">Richard Doe</a>

                <span class="datetime">at Jul 25, 2012 11:09</span>

													<span class="body">

													Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

													</span>

            </div>

        </li>

        <li class="in">

            <img class="avatar" alt="" src="media/image/avatar3.jpg" />

            <div class="message">

                <span class="arrow"></span>

                <a href="#" class="name">Richard Doe</a>

                <span class="datetime">at Jul 25, 2012 11:09</span>

													<span class="body">

													Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

													</span>

            </div>

        </li>

        <li class="out">

            <img class="avatar" alt="" src="media/image/avatar1.jpg" />

            <div class="message">

                <span class="arrow"></span>

                <a href="#" class="name">Bob Nilson</a>

                <span class="datetime">at Jul 25, 2012 11:09</span>

													<span class="body">

													Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

													</span>

            </div>

        </li>

        <li class="in">

            <img class="avatar" alt="" src="media/image/avatar3.jpg" />

            <div class="message">

                <span class="arrow"></span>

                <a href="#" class="name">Richard Doe</a>

                <span class="datetime">at Jul 25, 2012 11:09</span>

													<span class="body">

													Lorem ipsum dolor sit amet, consectetuer adipiscing elit,

													sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.

													</span>

            </div>

        </li>

        <li class="out">

            <img class="avatar" alt="" src="media/image/avatar1.jpg" />

            <div class="message">

                <span class="arrow"></span>

                <a href="#" class="name">Bob Nilson</a>

                <span class="datetime">at Jul 25, 2012 11:09</span>

													<span class="body">

													Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. sed diam nonummy nibh euismod tincidunt ut laoreet.

													</span>

            </div>

        </li>

    </ul>

</div>

<div class="chat-form">

    <div class="input-cont">

        <input class="m-wrap" type="text" placeholder="Type a message here..." />

    </div>

    <div class="btn-cont">

        <span class="arrow"></span>

        <a href="" class="btn blue icn-only"><i class="icon-ok icon-white"></i></a>

    </div>

</div>

</div>

</div>

<!-- END PORTLET-->

</div>

</div>

</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/date.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/daterangepicker.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.gritter.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.easy-pie-chart.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.sparkline.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>
<script src="<%=request.getContextPath()%>/themes/bootstrap/js/index.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>

    jQuery(document).ready(function() {

        App.init(); // initlayout and core plugins

        Index.init();

        Index.initCalendar(); // init index page's custom scripts

        Index.initCharts(); // init index page's custom scripts

        Index.initChat();

        Index.initMiniCharts();

        Index.initDashboardDaterange();

        Index.initIntro();

    });

</script>

<!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>