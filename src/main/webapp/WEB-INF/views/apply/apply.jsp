<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>医美通</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/select2_metro.css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/chosen.css" />

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/datepicker.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/multi-select-metro.css">

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/styles/custom_style.css">

    <!-- END PAGE LEVEL STYLES -->

</head>
<body >
<!--内容开始 -->
<div class="custon-main-container">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN STYLE CUSTOMIZER -->

        <div class="color-panel hidden-phone">

            <div class="color-mode-icons icon-color"></div>

            <div class="color-mode-icons icon-color-close"></div>

            <div class="color-mode">

                <p>THEME COLOR</p>

                <ul class="inline">

                    <li class="color-black current color-default" data-style="default"></li>

                    <li class="color-blue" data-style="blue"></li>

                    <li class="color-brown" data-style="brown"></li>

                    <li class="color-purple" data-style="purple"></li>

                    <li class="color-grey" data-style="grey"></li>

                    <li class="color-white color-light" data-style="light"></li>

                </ul>

                <label>

                    <span>Layout</span>

                    <select class="layout-option m-wrap small">

                        <option value="fluid" selected>Fluid</option>

                        <option value="boxed">Boxed</option>

                    </select>

                </label>

                <label>

                    <span>Header</span>

                    <select class="header-option m-wrap small">

                        <option value="fixed" selected>Fixed</option>

                        <option value="default">Default</option>

                    </select>

                </label>

                <label>

                    <span>Sidebar</span>

                    <select class="sidebar-option m-wrap small">

                        <option value="fixed">Fixed</option>

                        <option value="default" selected>Default</option>

                    </select>

                </label>

                <label>

                    <span>Footer</span>

                    <select class="footer-option m-wrap small">

                        <option value="fixed">Fixed</option>

                        <option value="default" selected>Default</option>

                    </select>

                </label>

            </div>

        </div>

        <!-- END BEGIN STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->

        <h3 class="page-title">

            立即注册成为我们的认证医院

        </h3>

        <!-- END PAGE TITLE & BREADCRUMB-->

    </div>

</div>

<!-- END PAGE HEADER-->
<div class="row-fluid">

<div class="span12">

<div class="portlet box blue" id="form_wizard_1">

<div class="portlet-title">

    <div class="caption">

        <i class="icon-reorder"></i> 添加医院信息

    </div>

</div>

<div class="portlet-body form">

<form action="hospital/doAdd" class="form-horizontal" id="submit_form">

<div class="form-wizard">

<div class="navbar steps">

    <div class="navbar-inner">

        <ul class="row-fluid">

            <li class="span3">

                <a href="#tab1" data-toggle="tab" class="step active">

                    <span class="number">第一步</span>

                    <span class="desc"><i class="icon-ok"></i>填写基本信息</span>

                </a>

            </li>

            <li class="span3">

                <a href="#tab2" data-toggle="tab" class="step">

                    <span class="number">第二步</span>

                    <span class="desc"><i class="icon-ok"></i>提交资质信息</span>

                </a>

            </li>

        </ul>

    </div>

</div>

<div id="bar" class="progress progress-success progress-striped">

    <div class="bar"></div>

</div>

<div class="tab-content">

<div class="alert alert-error hide">

    <button class="close" data-dismiss="alert"></button>

    验证不通过,请查看如下信息.

</div>

<div class="alert alert-success hide">

    <button class="close" data-dismiss="alert"></button>

    验证通过!

</div>

<div class="tab-pane active" id="tab1">

    <h3 class="block">请输入医院基本信息</h3>

    <div class="control-group">

        <label class="control-label">医院名称<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="hospitalName"/>

        </div>

    </div>

    <div class="control-group">

        <label class="control-label">医院别名</label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="password" id="submit_form_password"/>

        </div>

    </div>

    <div class="control-group">

        <label class="control-label">联系电话<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="phone"/>

        </div>

    </div>

    <div class="control-group">

        <label class="control-label">所在地区<span class="required">*</span></label>

        <div class="controls">

            <select id="seachprov" name="seachprov" class="span3" onchange="changeComplexProvince(this.value, sub_array, 'seachcity', 'seachdistrict');">
            </select>
            &nbsp;&nbsp;
            <select id="seachcity" name="homecity" class="span3"  onchange="changeCity(this.value,'seachdistrict','seachdistrict');">
            </select>
            &nbsp;&nbsp;
                <span id="seachdistrict_div">
                    <select id="seachdistrict" name="seachdistrict">
                    </select>
                </span>

        </div>

    </div>

    <div class="control-group">

        <label class="control-label">医院地址<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="hospitalAddress"  placeholder="请输入医院地址..."/>

        </div>

    </div>

    <div class="control-group">

        <label class="control-label">医院属性<span class="required">*</span></label>

        <div class="controls">

            <select name="country" id="hospitalList" class="span6">
                <option value="1">公立医院</option>
                <option value="2">私立医院</option>
            </select>

        </div>

    </div>

    <div class="control-group">

        <label class="control-label">项目分级<span class="required">*</span></label>

        <div class="controls">

            <select name="country" id="levelList" class="span6">
                <option value="1">一级</option>
                <option value="2">二级</option>
                <option value="3">三级</option>
            </select>

        </div>

    </div>

    <div class="control-group">

        <label class="control-label">医院网址<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="address" />

        </div>

    </div>

    <div class="control-group">

        <label class="control-label">特色项目<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="city"/>

        </div>

    </div>

</div>

<div class="tab-pane" id="tab2">

    <div class="control-group">

        <label class="control-label">医院资质:<span class="required">*</span></label>

        <div class="controls">

            <div class="fileupload fileupload-new" data-provides="fileupload">

                <div class="input-append">

                    <div class="uneditable-input">

                        <i class="icon-file fileupload-exists"></i>

                        <span class="fileupload-preview"></span>

                    </div>

                                                        <span class="btn btn-file">

                                                        <span class="fileupload-new">选择文件</span>

                                                        <span class="fileupload-exists">更换</span>

                                                        <input type="file" class="default" />

                                                        </span>

                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">清除</a>

                </div>

            </div>

        </div>

    </div>

    <h3 class="block">输入联系人信息</h3>

    <div class="control-group">

        <label class="control-label">姓名:<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="contactsName"  placeholder="请输入姓名"/>

        </div>

    </div>
    <div class="control-group">

        <label class="control-label">性别:<span class="required">*</span></label>

        <div class="controls">

            <label class="radio">

                <input type="radio" name="optionsRadios1" value="1" checked />   女

            </label>

            <label class="radio">

                <input type="radio" name="optionsRadios1" value="0"  />男

            </label>

        </div>

    </div>
    <div class="control-group">

        <label class="control-label">手机:<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="mobilePhone"  placeholder="请输入手机号"/>

        </div>

    </div>
    <div class="control-group">

        <label class="control-label">职位:<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="fullname"  placeholder="请输入职位"/>

        </div>

    </div>
    <div class="control-group">

        <label class="control-label">Email:<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="fullname"  placeholder="请输入邮箱地址"/>

        </div>

    </div>
    <div class="control-group">

        <label class="control-label">QQ:<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="fullname"  placeholder="请输入QQ"/>

        </div>

    </div>
    <div class="control-group">

        <label class="control-label">微信:<span class="required">*</span></label>

        <div class="controls">

            <input type="text" class="span6 m-wrap" name="wechat"  placeholder="请输入微信号"/>

        </div>

    </div>

</div>

</div>

<div class="form-actions clearfix">

    <a href="javascript:;" class="btn button-previous">

        <i class="m-icon-swapleft"></i>上一步

    </a>

    <a href="javascript:;" class="btn blue button-next">

        下一步 <i class="m-icon-swapright m-icon-white"></i>

    </a>

    <a href="javascript:;" class="btn green button-submit">

        提交 <i class="m-icon-swapright m-icon-white"></i>

    </a>
</div>

</div>

</form>

</div>

</div>

</div>

</div>


</div>

<!--内容结束 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-fileupload.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/additional-methods.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/select2.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/date.js"></script>


<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>

<script src="<%=request.getContextPath()%>/script/hospital.js"></script>

<script src="<%=request.getContextPath()%>/script/Area.js"></script>

<script src="<%=request.getContextPath()%>/script/AreaData_min.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>

    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();

        FormWizard.init();

        initComplexArea('seachprov', 'seachcity', 'seachdistrict', area_array, sub_array, '44', '0', '0');

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl : App.isRTL(),
                format : "yyyy-mm-dd"
            });
        }

        $('#add_hospital_btn').click(function(){
            window.location = '<%=request.getContextPath()%>/hospital/toAdd';
        });
    });

    //得到地区码
    function  getAreaID() {
        var area = 0;
        if ($("#seachdistrict").val() != "0") {
            area = $("#seachdistrict").val();
        }
        else if ($("#seachcity").val() != "0") {
            area = $("#seachcity").val();
        }
        else {
            area = $("#seachprov").val();
        }

        return area;
    }

    function showAreaID() {
        //地区码
        var areaID = getAreaID();
        //地区名
        var areaName = getAreaNamebyID(areaID) ;
        alert("您选择的地区码：" + areaID + "      地区名：" + areaName);
    }

    //根据地区码查询地区名
    function getAreaNamebyID(areaID) {
        var areaName = "";
        if (areaID.length == 2) {
            areaName = area_array[areaID];
        }
        else if (areaID.length == 4) {
            var index1 = areaID.substring(0, 2);
            areaName = area_array[index1] + " " + sub_array[index1][areaID];
        }
        else if (areaID.length == 6) {
            var index1 = areaID.substring(0, 2);
            var index2 = areaID.substring(0, 4);
            areaName = area_array[index1] + " " + sub_array[index1][index2] + " " + sub_arr[index2][areaID];
        }
        return areaName;
    }

</script>

</body>
</html>