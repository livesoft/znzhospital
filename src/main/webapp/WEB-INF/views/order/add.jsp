<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>创建订单</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/select2_metro.css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/chosen.css" />

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/datepicker.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/multi-select-metro.css">

    <!-- END PAGE LEVEL STYLES -->
    <style>
        .form-horizontal .control-label  {
            width: 80px;
        }
        .form-horizontal .controls {
            margin-left: 100px;
        }
    </style>

</head>
<body >
<!--内容开始 -->
<div class="row-fluid">

    <div class="span12">

        <div class="portlet box blue" id="form_wizard_1">

        <div class="portlet-title">

            <div class="caption">

                <i class="icon-reorder"></i> 添加订单信息

            </div>

        </div>

        <div class="portlet-body form">

        <form action="order/doAdd" class="form-horizontal" id="submit_form">

        <div class="form-wizard">

        <div class="navbar steps">

            <div class="navbar-inner">

                <ul class="row-fluid">

                    <li class="span3">

                        <a href="#tab1" data-toggle="tab" class="step active">

                            <span class="number">第一步</span>

                            <span class="desc"><i class="icon-ok"></i>客户基本信息</span>

                        </a>

                    </li>

                    <li class="span3">

                        <a href="#tab3" data-toggle="tab" class="step">

                            <span class="number">第二步</span>

                            <span class="desc"><i class="icon-ok"></i>配置项目</span>

                        </a>

                    </li>

                    <li class="span3">

                        <a href="#tab2" data-toggle="tab" class="step">

                            <span class="number">第三步</span>

                            <span class="desc"><i class="icon-ok"></i>分配医院</span>

                        </a>

                    </li>



                    <li class="span3">

                        <a href="#tab4" data-toggle="tab" class="step">

                            <span class="number">第四步</span>

                            <span class="desc"><i class="icon-ok"></i>确认</span>

                        </a>

                    </li>

                </ul>

            </div>

        </div>

        <div id="bar" class="progress progress-success progress-striped">

            <div class="bar"></div>

        </div>

        <div class="tab-content">

        <div class="alert alert-error hide">

            <button class="close" data-dismiss="alert"></button>

            验证不通过,请查看如下信息.

        </div>

        <div class="alert alert-success hide">

            <button class="close" data-dismiss="alert"></button>

            验证通过!

        </div>

        <div class="tab-pane active" id="tab1">

            <h3 class="block">请输入顾客基本信息</h3>

            <div class="control-group">

                <label class="control-label">姓名<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="username" id="username"/>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">性别:<span class="required">*</span></label>

                <div class="controls">

                    <label class="radio">

                        <input type="radio" name="sex" value="1" id="female" checked />   <label for="female">女</label>

                    </label>

                    <label class="radio">

                        <input type="radio" name="sex" value="0" id="male" /> <label for="male">男</label>

                    </label>

                </div>

            </div>


            <div class="control-group">

                <label class="control-label">年龄<span class="required">*</span></label>

                <div class="controls">

                    <input type="number" class="span6 m-wrap" name="age" id="age"/>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">电话<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="tel" id="tel"/>

                </div>

            </div>


            <div class="control-group">

                <label class="control-label">地区<span class="required">*</span></label>

                <div class="controls">

                    <select id="seachprov" name="addressseachprov" class="span3" onchange="changeComplexProvince(this.value, sub_array, 'seachcity', 'seachdistrict');">
                    </select>
                    &nbsp;&nbsp;
                    <select id="seachcity" name="addresshomecity" class="span3"  onchange="changeCity(this.value,'seachdistrict','seachdistrict');">
                    </select>
                    &nbsp;&nbsp;
                     <span id="seachdistrict_div">
                        <select id="seachdistrict" name="addressseachdistrict"></select>
                     </span>

                </div>

            </div>


            <div class="control-group">

                <label class="control-label">美丽留言<span class="required">*</span></label>

                <div class="controls">

                    <textarea  class="large m-wrap" name="userMessage" id="userMessage" ></textarea>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">备注<span class="required">*</span></label>

                <div class="controls">

                    <textarea  class="large m-wrap" name="note" id="note" ></textarea>

                </div>

            </div>


        </div>

        <div class="tab-pane" id="tab2">

            <h3 class="block">选择医院</h3>
            <div class="control-group">

                <label class="control-label">医院<span class="required">*</span></label>

                <div class="controls">
                    <div class="row-fluid">
                        <div class="span7 m-wrap">
                            <table class="table table-bordered table-hover" id="hospitalTable">
                                <thead>
                                <tr>
                                    <th>医院名称</th>
                                    <th>特色项目</th>
                                    <th>联系电话</th>
                                    <th>状态</th>
                                    <th>分派项目</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="span5 m-wrap">
                            <div class="row-fluid">
                                <input type="text" id="hospitalName" class="span3 m-wrap" placeholder="请输入医院名称"/>
                                <select id="provincelist" class="span3 m-wrap" onchange="changeComplexProvince(this.value, sub_array, 'citylist', null);">
                                </select>
                                <select id="citylist" class="span3 m-wrap">
                                </select>
                                <select id="countylist" class="span3 m-wrap" style="display: none;">
                                </select>
                                <button type="button" class="btn blue" onclick="refreshHospital();">查询</button>
                            </div>

                            <div class="row-fluid">
                                <table class="table table-bordered table-hover" id="hospitalListTable" >
                                    <thead>
                                    <tr>
                                        <th>医院名称</th>
                                        <th>特色项目</th>
                                        <th>联系电话</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="hosp" items="${hospitalArray}">
                                        <tr>
                                           <td>${hosp.name}</td>
                                           <td>
                                               <c:forEach var="pro" items="${hosp.featureProject}" varStatus="hospStatus">
                                                  <c:if test="${hospStatus.index>0}">,</c:if>
                                                  ${pro.projectName}
                                               </c:forEach>
                                           </td>
                                           <td>
                                               ${hosp.tel}
                                           </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="tab-pane" id="tab3">

            <h3 class="block">配置项目</h3>

            <div class="control-group">

                <label class="control-label">项目<span class="required">*</span></label>

                <div class="controls">
                    <div class="row-fluid">
                        <div class="span6 m-wrap">
                            <table class="table table-bordered table-hover" id="proTable">
                                <thead>
                                <tr>
                                    <th>项目名称</th>
                                    <th>最低消费</th>
                                    <th>最高消费</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="span6 m-wrap">
                            <select id="partlist" class="span7 m-wrap" onchange="changePart()">
                                <c:forEach var="part" items="${partArray}">
                                   <option value="${part.id}">${part.name}</option>
                                </c:forEach>
                            </select>
                            <select id="parlistDetail" class="span7 m-wrap" multiple="multiple" data-placeholder="选择项目" tabindex="1">
                                <c:forEach var="pro" items="${projectArray}">
                                   <option value="${pro.id}">${pro.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

            </div>


        </div>

        <div class="tab-pane" id="tab4">

            <h3 class="block">确认输入信息</h3>

            <h4 class="form-section">订单详情</h4>

            <div class="control-group">

                <label class="control-label">顾客姓名:</label>

                <div class="controls">

                    <span class="text display-value" data-display="username"></span>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">顾客性别:</label>

                <div class="controls">

                    <span class="text display-value" data-display="sex"></span>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">顾客年龄:</label>

                <div class="controls">

                    <span class="text display-value" data-display="age"></span>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">顾客电话:</label>

                <div class="controls">

                    <span class="text display-value" data-display="tel"></span>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">顾客地址:</label>

                <div class="controls">

                    <span class="text display-value" data-display="address"></span>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">美丽留言:</label>

                <div class="controls">

                    <span class="text display-value" data-display="note"></span>

                </div>

            </div>


            <div class="control-group">

                <label class="control-label">项目:</label>

                <div class="controls">

                    <span class="text display-value" data-display="proTable"></span>

                </div>

            </div>


            <div class="control-group">

                <label class="control-label">分配医院:</label>

                <div class="controls">

                    <span class="text display-value" data-display="hospitalTable"></span>

                </div>

            </div>

        </div>

        </div>

        <div class="form-actions clearfix">

            <a href="javascript:;" class="btn button-previous">

                <i class="m-icon-swapleft"></i>上一步

            </a>

            <a href="javascript:;" class="btn blue button-next">

                下一步 <i class="m-icon-swapright m-icon-white"></i>

            </a>

            <a href="javascript:;" class="btn green button-submit">

                提交 <i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

        </div>

        </form>

        </div>

        </div>

    </div>

</div>


<!--内容结束 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-fileupload.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/additional-methods.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/select2.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/date.js"></script>


<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>

<script src="<%=request.getContextPath()%>/script/order.js"></script>

<script src="<%=request.getContextPath()%>/script/Area.js"></script>

<script src="<%=request.getContextPath()%>/script/AreaData_min.js"></script>

<script src="<%=request.getContextPath()%>/script/common.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>

    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();

        FormWizard.init();

        initComplexArea('seachprov', 'seachcity', 'seachdistrict', area_array, sub_array, '44', '0', '0');

        initComplexArea('provincelist', 'citylist', 'countylist', area_array, sub_array, '44', '0', '0');

        $("#parlistDetail").live("click",function(){
            var objValue=$("#parlistDetail option:selected").text();
            var i=0;
            $("#proTable").find("tr").each(function(){
                var tempVal=$(this).children("td").eq(0).html();
                if(tempVal==objValue){
                    alert("此条项目已经被选择过了!");
                    i++;
                    return;
                }
            });
            if(i==0){
                $("#proTable").append("<tr><td>"+objValue+"</td><td><input type='number' class='span6 m-wrap' /></td><td><input type='number' class='span6 m-wrap' /></td><td><span class='label label-success' onclick='deleteRow(this)'>删除</span></td></tr>");
            }
        });

        $("#hospitalListTable>tbody>tr").live("click",function(){
            var objValue=$(this).children("td").eq(0).html();
            var choose=false;
            $("#hospitalTable>tbody>tr").each(function(){
                var tempVal=$(this).children("td").eq(0).html();
                if(tempVal==objValue){
                    alert("该医院已经被选择过了!");
                    choose=true;
                    return;
                }
            });
            if(!choose){

                var clonetr=$(this).clone();
                var projects=new Array();
                $("#proTable>tbody>tr").each(function(){
                    console.info(this);
                    projects.push($(this).children("td").eq(0).html());
                });

                var tdd='<input type="hidden" class="span12 select2_sample3" value="'+projects.join(",")+'">';

                var app=clonetr.append("<td>未接单</td><td>"+tdd+"</td><td><span class='label label-success' onclick='deleteRow(this)'>删除</span></td>");
                $("#hospitalTable>tbody").append(app);
                $(".select2_sample3").select2({
                    tags:projects
                });
            }

        });

    });

    //得到地区码
    function  getAreaID() {
        var area = 0;
        if ($("#seachdistrict").val() != "0") {
            area = $("#seachdistrict").val();
        }
        else if ($("#seachcity").val() != "0") {
            area = $("#seachcity").val();
        }
        else {
            area = $("#seachprov").val();
        }

        return area;
    }

    function showAreaID() {
        //地区码
        var areaID = getAreaID();
        //地区名
        var areaName = getAreaNamebyID(areaID) ;
        alert("您选择的地区码：" + areaID + "      地区名：" + areaName);
    }

    //根据地区码查询地区名
    function getAreaNamebyID(areaID) {
        var areaName = "";
        if (areaID.length == 2) {
            areaName = area_array[areaID];
        }
        else if (areaID.length == 4) {
            var index1 = areaID.substring(0, 2);
            areaName = area_array[index1] + " " + sub_array[index1][areaID];
        }
        else if (areaID.length == 6) {
            var index1 = areaID.substring(0, 2);
            var index2 = areaID.substring(0, 4);
            areaName = area_array[index1] + " " + sub_array[index1][index2] + " " + sub_arr[index2][areaID];
        }
        return areaName;
    }
    function beforeRefreshHospital(){
        $("#hospitalListTable>tbody").empty();
    }
    function callbcakRefreshHospital(cb){
        $(cb).each(function(){
            var tr="<tr><td>"+this.name+"</td><td>";
            var temp=new Array();
            if(this.featureProject!=null){
                for(var project in this.featureProject){
                    temp.push(this.featureProject[project].projectName);
                }
                tr+=temp.join("、");
            }
            tr+="</td><td>"+this.tel+"</td></tr>";
            $("#hospitalListTable>tbody").append(tr);
        });
    }
    function refreshHospital(){
        var provinceid=$("#provincelist option:selected").val();
        var cityid=$("#citylist option:selected").val();
        var hospitalName=$("#hospitalName").val();
        getData("POST","getHospitalAllByCityId","cityId="+cityid+"&provinceid="+provinceid+"&hospitalName="+hospitalName,"beforeRefreshHospital",null,"callbcakRefreshHospital");
    }
    function beforeChangePart(){
        $("#parlistDetail").empty();
    }
    function callbcakChangePart(cb){
        $(cb).each(function(){
            $("#parlistDetail").append("<option value="+this.id+">"+this.name+"</option>");
        });
    }
    function  changePart(){
        var obj=$("#partlist option:selected").val();
        getData("POST","getProjectByPartId","partId="+obj,"beforeChangePart",null,"callbcakChangePart");
    }

    function deleteRow(obj){
        $(obj).parent().parent().remove();
    }

</script>

</body>
</html>