<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

    <meta charset="utf-8" />

    <title>Metronic | Form Stuff - Advance Form Samples</title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <meta content="" name="description" />

    <meta content="" name="author" />

    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/style-metro.css" rel="stylesheet" type="text/css"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/style.css" rel="stylesheet" type="text/css"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/style-responsive.css" rel="stylesheet" type="text/css"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/default.css" rel="stylesheet" type="text/css" id="style_color"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/uniform.default.css" rel="stylesheet" type="text/css"/>

    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/select2_metro.css" />

    <!-- END PAGE LEVEL SCRIPTS -->

    <link rel="shortcut icon" href="<%=request.getContextPath()%>/themes/bootstrap/image/favicon.ico" />

</head>

<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class="page-header-fixed">

<!-- BEGIN PAGE CONTENT-->

<div class="row-fluid">

<div class="span12">

<div class="tabbable tabbable-custom boxless">

<div class="tab-content">


<div class="" id="tab_3">

<div class="portlet box blue">

<div class="portlet-title">

    <div class="caption"><i class="icon-reorder"></i>订单详情</div>

    <div class="tools">

        <a href="javascript:;" class="collapse"></a>

        <a href="#portlet-config" data-toggle="modal" class="config"></a>

        <a href="javascript:;" class="reload"></a>

        <a href="javascript:;" class="remove"></a>

    </div>

</div>

<div class="portlet-body form">

<!-- BEGIN FORM-->

<div class="form-horizontal form-view">

<h3 class="form-section">客户基本信息</h3>

<div class="row-fluid">

    <div class="span6 ">

        <div class="control-group">

            <label class="control-label" for="firstName">姓名:</label>

            <div class="controls">

                <span class="text">张三</span>

            </div>

        </div>

    </div>

    <!--/span-->

    <div class="span6 ">

        <div class="control-group">

            <label class="control-label" for="lastName">电话:</label>

            <div class="controls">

                <span class="text">13581683143</span>

            </div>

        </div>

    </div>

    <!--/span-->

</div>

<!--/row-->

<div class="row-fluid">

    <div class="span6 ">

        <div class="control-group">

            <label class="control-label" >性别:</label>

            <div class="controls">

                <span class="text">女</span>

            </div>

        </div>

    </div>

    <!--/span-->

    <div class="span6 ">

        <div class="control-group">

            <label class="control-label" >生日:</label>

            <div class="controls">

                <span class="text bold">20.01.1984</span>

            </div>

        </div>

    </div>

    <!--/span-->

</div>

<!--/row-->

<div class="row-fluid">

    <div class="span6 ">

        <div class="control-group">

            <label class="control-label" >城市:</label>

            <div class="controls">

                <span class="text bold">北京</span>

            </div>

        </div>

    </div>

    <!--/span-->

    <div class="span6 ">

        <div class="control-group">

            <label class="control-label" >项目:</label>

            <div class="controls">

                <span class="text bold">丰胸、隆鼻</span>

            </div>

        </div>

    </div>

    <!--/span-->

</div>

<!--/row-->

<h3 class="form-section">操作</h3>

<div class="row-fluid ">

<div class="span12">

    <!-- BEGIN TAB PORTLET-->

    <div class="portlet box green tabbable">

        <div class="portlet-title">

            <div class="caption"><i class="icon-reorder"></i>操作</div>

        </div>

        <div class="portlet-body">

            <div class="tabbable portlet-tabs">

                <ul class="nav nav-tabs">

                    <li class="active"><a href="#portlet_tab3" data-toggle="tab">备注</a></li>

                    <li><a href="#portlet_tab2" data-toggle="tab">成单</a></li>

                    <li><a href="#portlet_tab1" data-toggle="tab">撞单</a></li>

                </ul>

                <div class="tab-content">

                    <div class="tab-pane" id="portlet_tab1">

                        <div class="control-group">

                            <label class="control-label">撞单图片</label>

                            <div class="controls">

                                <input type="file" class="default" />

                            </div>

                        </div>
                        <div class="control-group">

                            <label class="control-label">备注<span class="required">*</span></label>

                            <div class="controls">

                                <textarea  class="large m-wrap" name="note" ></textarea>

                            </div>

                        </div>

                    </div>

                    <div class="tab-pane" id="portlet_tab2">

                        <div class="control-group">

                            <label class="control-label">手术时间</label>

                            <div class="controls">

                                <div class="input-append date form_datetime">

                                    <input size="16" type="text" value="" readonly class="m-wrap">

                                    <span class="add-on"><i class="icon-calendar"></i></span>

                                </div>

                            </div>

                        </div>
                        <div class="control-group">

                            <label class="control-label">消费金额<span class="required">*</span></label>

                            <div class="controls">

                                <input type="text" class="span6 m-wrap" name="money"/>

                            </div>

                        </div>
                        <div class="control-group">

                            <label class="control-label">备注<span class="required">*</span></label>

                            <div class="controls">

                                <textarea  class="large m-wrap" name="note" ></textarea>

                            </div>

                        </div>


                    </div>

                    <div class="tab-pane  active" id="portlet_tab3">
                        <div class="control-group">

                            <label class="control-label">备注<span class="required">*</span></label>

                            <div class="controls">

                                <textarea  class="large m-wrap" name="note" ></textarea>

                            </div>

                        </div>


                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- END TAB PORTLET-->

</div>


</div>

<div class="form-actions">

    <button type="submit" class="btn blue"><i class="icon-pencil"></i> Edit</button>

    <button type="button" class="btn">Back</button>

</div>

</div>

<!-- END FORM-->

</div>

</div>

</div>


</div>

</div>

</div>

</div>

<!-- END PAGE CONTENT-->



<!-- BEGIN FOOTER -->

<div class="footer">

    <div class="footer-inner">

        2013 &copy; Metronic by keenthemes.Collect from <a href="http://www.cssmoban.com/" title="网站模板" target="_blank">网站模板</a> - More Templates <a href="http://www.cssmoban.com/" target="_blank" title="模板之家">模板之家</a>

    </div>

    <div class="footer-tools">

			<span class="go-top">

			<i class="icon-angle-up"></i>

			</span>

    </div>

</div>

<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery-1.10.1.min.js" type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<!--[if lt IE 9]>

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/excanvas.min.js"></script>

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/respond.min.js"></script>

<![endif]-->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.slimscroll.min.js" type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.blockui.min.js" type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.cookie.min.js" type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.uniform.min.js" type="text/javascript" ></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/select2.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/form-samples.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->

<script>

    jQuery(document).ready(function() {

        // initiate layout and plugins

        App.init();

        FormSamples.init();

    });

</script>

<!-- END JAVASCRIPTS -->

</body>

<!-- END BODY -->

</html>