<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>美丽需求列表</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->

    <link rel="stylesheet" href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-table.css">

    <!-- END PAGE LEVEL STYLES -->

    <link rel="shortcut icon" href="<%=request.getContextPath()%>/themes/bootstrap/image/favicon.ico" />
</head>
<body class="page-header-fixed">

<!--内容部分-->

<div class="portlet box light-grey">

    <div class="portlet-title">

        <div class="caption"><i class="icon-globe"></i>美丽需求信息列表</div>

    </div>

    <div class="portlet-body">

        <div class="clearfix">

            <div class="btn-group pull-right">

                <button class="btn dropdown-toggle" data-toggle="dropdown">导出/打印 <i class="icon-angle-down"></i>

                </button>
                <ul class="dropdown-menu pull-right">

                    <li><a href="#">打印</a></li>

                    <li><a href="#">导出Excel</a></li>

                </ul>

            </div>

        </div>

        <table id="demand-tb1" data-url="<%=request.getContextPath()%>/demand/list" data-height="399" data-side-pagination="server"  data-pagination="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-search="true">
            <thead>
            <tr>
                <th data-field="state" data-checkbox="true"></th>
                <th data-field="userName" data-sortable="true" class="span3">顾客名称</th>
                <th data-field="accountName" data-sortable="true" class="span3">性别</th>
                <th data-field="phone" data-formatter="typeFormatter" data-sortable="true" class="span2">需求项目</th>
                <th data-field="userLevel" data-formatter="levelFormatter" data-sortable="true" class="span2">状态</th>
                <th data-field="operate" data-formatter="operateFormatter" data-events="operateEvents" class="span2">操作</th>
            </tr>
            </thead>
        </table>

    </div>

</div>

<!--end内容部分-->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-table.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-table-zh-CN.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/DT_bootstrap.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>

<script>

    jQuery(document).ready(function() {
        App.init();

        $('#demand-tb1').bootstrapTable({
            url: '<%=request.getContextPath()%>/user/list'
        });
        $('#add_demand_btn').click(function(){
            window.location = '<%=request.getContextPath()%>/user/toAdd';
        });
    });

</script>

</body>
</html>