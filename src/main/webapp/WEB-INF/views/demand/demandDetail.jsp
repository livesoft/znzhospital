<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>需求信息管理</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/select2_metro.css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/chosen.css" />

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/datepicker.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/multi-select-metro.css">

    <!-- END PAGE LEVEL STYLES -->

</head>
<body >
<!--内容开始 -->
<div class="row-fluid">

    <div class="span12">

        <div class="portlet box blue" id="form_wizard_1">

        <div class="portlet-title">

            <div class="caption">

                <i class="icon-reorder"></i> 美丽需求信息

            </div>

        </div>

        <div class="portlet-body form">


        <div class="form-wizard">

            <div class="control-group">

                <label class="control-label">客户姓名<span class="required">*</span></label>

                <div class="controls">
                             王美丽
                </div>

            </div>

            <div class="control-group">

                <label class="control-label">客户性别</label>

                <div class="controls">

                    男

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">联系电话<span class="required">*</span></label>

                <div class="controls">

                    13566622277

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">所在地区<span class="required">*</span></label>

                <div class="controls">
                        北京市,海淀区
                </div>

            </div>

            <div class="control-group">

                <label class="control-label">需求状态<span class="required">*</span></label>

                <div class="controls">
                       新需求
                </div>

            </div>

            <div class="control-group">

                <label class="control-label">需求提交日期<span class="required">*</span></label>

                <div class="controls">

                     2015-05-10 12:10:00

                </div>

            </div>

            <div class="form-actions">

                <button type="button" class="btn blue">下单</button>

                <button type="button" class="btn">返回</button>

            </div>

        </div>

        </div>

        </div>

    </div>

</div>

<!--内容结束 -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-fileupload.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/additional-methods.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/select2.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/date.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>


<!-- END PAGE LEVEL SCRIPTS -->
<script>

    jQuery(document).ready(function() {

        App.init();

    });
</script>

</body>
</html>