<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>医院信息管理</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->

    <link rel="stylesheet" href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-table.css">

    <!-- END PAGE LEVEL STYLES -->

    <link rel="shortcut icon" href="<%=request.getContextPath()%>/themes/bootstrap/image/favicon.ico" />
</head>
<body class="page-header-fixed">

<!--内容部分-->

<div class="portlet box light-grey">

    <div class="portlet-title">

        <div class="caption"><i class="icon-globe"></i>医院信息列表</div>

    </div>

    <div class="portlet-body">

        <div class="clearfix">

            <div class="btn-group">

                <button id="add_hospital_btn" class="btn green">

                    新增医院 <i class="icon-plus"></i>

                </button>

            </div>

            <div class="btn-group pull-right">

                <button class="btn dropdown-toggle" data-toggle="dropdown">导出/打印 <i class="icon-angle-down"></i>

                </button>
                <ul class="dropdown-menu pull-right">

                    <li><a href="#">打印</a></li>

                    <li><a href="#">导出Excel</a></li>

                </ul>

            </div>

        </div>

        <table id="hospital-tb1" data-url="<%=request.getContextPath()%>/hospital/list" data-height="399" data-side-pagination="server"  data-pagination="true" data-page-list="[5, 10, 20, 50, 100, 200]" data-search="true">
            <thead>
            <tr>
                <th data-field="state" data-checkbox="true"></th>
                <th data-field="resourceName" data-sortable="true" class="span3">医院名称</th>
                <th data-field="resourceUrl" data-sortable="true" class="span3">医院别名</th>
                <th data-field="resourceType" data-formatter="typeFormatter" data-sortable="true" class="span2">联系方式</th>
                <th data-field="resourceLevel" data-formatter="levelFormatter" data-sortable="true" class="span2">项目分级</th>
                <th data-field="resourceLevel" data-formatter="levelFormatter" data-sortable="true" class="span2">状态</th>
                <th data-field="operate" data-formatter="operateFormatter" data-events="operateEvents" class="span2">操作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="hosp" items="${hospitalArray}">
               <tr>
                  <td><input type="checkbox" value="${hosp.id}"/></td>
                  <td>${hosp.hospitalName}</td>
                  <td>${hosp.hospitalAlias}</td>
                  <td>${hosp.hospitalPhone}</td>
                  <td>${hosp.projectLevel.name}<input type="hidden" value="${hosp.projectLevel.id}"/></td>
                  <td>${hosp.state}</td>
                  <td><a href="javaScript:void(0)">删除</a>  <a href="javaScript:void(0)">修改</a></td>
               </tr>
            </c:forEach>
            </tbody>
        </table>

    </div>

</div>

<!--end内容部分-->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-table.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-table-zh-CN.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/select2.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/DT_bootstrap.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>

<script>

    jQuery(document).ready(function() {
        App.init();

        $('#hospital-tb1').bootstrapTable({
            url: '<%=request.getContextPath()%>/hospital/list'
        });
        $('#add_hospital_btn').click(function(){
            window.location = '<%=request.getContextPath()%>/hospital/toAdd';
        });
    });

</script>

</body>
</html>