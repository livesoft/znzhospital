<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>医院信息管理</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/select2_metro.css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/chosen.css" />

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/datepicker.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/multi-select-metro.css">

    <!-- END PAGE LEVEL STYLES -->

</head>
<body >
<!--内容开始 -->
<c:set var="ctx" value="<%=request.getContextPath()%>"/>
<div class="row-fluid">

    <div class="span12">

        <div class="portlet box blue" id="form_wizard_1">

        <div class="portlet-title">

            <div class="caption">

                <i class="icon-reorder"></i> 添加医院信息

            </div>

        </div>

        <div class="portlet-body form">

        <form action="hospital/doAdd" class="form-horizontal" id="submit_form">

        <div class="form-wizard">

            <div class="control-group">

                <label class="control-label">医院名称<span class="required">*</span></label>

                <div class="controls">
                      s北京东方瑞丽
                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院别名</label>

                <div class="controls">

                    s北京东方瑞丽

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">联系电话<span class="required">*</span></label>

                <div class="controls">

                    13566622277

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">所在地区<span class="required">*</span></label>

                <div class="controls">
                        北京市,海淀区
                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院地址<span class="required">*</span></label>

                <div class="controls">

                    b北京市海淀区苏州街33号公寓

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院属性<span class="required">*</span></label>

                <div class="controls">

                     s私立医院

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">项目分级<span class="required">*</span></label>

                <div class="controls">

                    s三级

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院网址<span class="required">*</span></label>

                <div class="controls">

                    www.bjdfrlzx.com

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">特色项目<span class="required">*</span></label>

                <div class="controls">

                    b自体耳软骨垫鼻梁

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">合约项目<span class="required">*</span></label>

                <div class="controls">
                    <div class="row-fluid">
                        <div class="span4 m-wrap">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>项目名称</th>
                                    <th>设置提成比例(%)</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>双眼皮</td>
                                    <td></td>
                                    <td><span class="label label-success">删除</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="span4 m-wrap">
                            <select id="partlist" class="span7 m-wrap" onchange="changePart()">
                                <c:forEach var="part" items="${partArray}">
                                     <option value="${part.id}">${part.name}</option>
                                </c:forEach>
                            </select>

                            <select class="span7 m-wrap" multiple="multiple" data-placeholder="选择项目" tabindex="1">
                                <c:forEach var="pro" items="${projectArray}">
                                     <option value="${pro.id}">${pro.projectName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="control-group">

                <label class="control-label">合同到期日期<span class="required">*</span></label>

                <div class="controls">

                    <div class="input-append date date-picker" data-date="2015-10-10" data-date-format="yyyy-mm-dd" data-date-viewmode="years">

                        <input class="m-wrap m-ctrl-medium date-picker" readonly size="16" type="text" value="" /><span class="add-on"><i class="icon-calendar"></i></span>

                    </div>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">认证证明<span class="required">*</span></label>

                <div class="controls">

                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <div class="input-append">

                            <div class="uneditable-input">

                                <i class="icon-file fileupload-exists"></i>

                                <span class="fileupload-preview"></span>

                            </div>

                                                        <span class="btn btn-file">

                                                        <span class="fileupload-new">选择文件</span>

                                                        <span class="fileupload-exists">更换</span>

                                                        <input type="file" class="default" />

                                                        </span>

                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">清除</a>

                        </div>

                    </div>

                </div>

            </div>

            <div class="form-actions">

                <button type="button" class="btn blue">确定</button>

                <button type="button" class="btn">取消</button>

            </div>

        </div>

        </form>

        </div>

        </div>

    </div>

</div>


<!--内容结束 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-fileupload.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/additional-methods.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/select2.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/date.js"></script>


<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>

<script src="<%=request.getContextPath()%>/script/hospital.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>

    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();

        FormWizard.init();

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl : App.isRTL(),
                format : "yyyy-mm-dd"
            });
        }

        $('#add_hospital_btn').click(function(){
            window.location = '<%=request.getContextPath()%>/hospital/toAdd';
        });
    });

    //得到地区码
    function  getAreaID() {
        var area = 0;
        if ($("#seachdistrict").val() != "0") {
            area = $("#seachdistrict").val();
        }
        else if ($("#seachcity").val() != "0") {
            area = $("#seachcity").val();
        }
        else {
            area = $("#seachprov").val();
        }

        return area;
    }

    function showAreaID() {
        //地区码
        var areaID = getAreaID();
        //地区名
        var areaName = getAreaNamebyID(areaID) ;
        alert("您选择的地区码：" + areaID + "      地区名：" + areaName);
    }

    //根据地区码查询地区名
    function getAreaNamebyID(areaID) {
        var areaName = "";
        if (areaID.length == 2) {
            areaName = area_array[areaID];
        }
        else if (areaID.length == 4) {
            var index1 = areaID.substring(0, 2);
            areaName = area_array[index1] + " " + sub_array[index1][areaID];
        }
        else if (areaID.length == 6) {
            var index1 = areaID.substring(0, 2);
            var index2 = areaID.substring(0, 4);
            areaName = area_array[index1] + " " + sub_array[index1][index2] + " " + sub_arr[index2][areaID];
        }
        return areaName;
    }
    function  changePart(){
        var partId=$("#partlist option:selected").val();
        var url="${ctx}/hospital/getPorjectListByPart?partId="+partId;
        $.ajax({
            type:"GET",
            url:url,
            dataType:"text",
            success:function(data){
                if(data){

                }
            }
        });
    }
</script>

</body>
</html>