<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>医院信息管理</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/select2_metro.css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/chosen.css" />

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/datepicker.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/multi-select-metro.css">

    <!-- END PAGE LEVEL STYLES -->

</head>
<body >
<!--内容开始 -->
<c:set var="ctx" value="<%=request.getContextPath()%>"/>
<div class="row-fluid">

    <div class="span12">

        <div class="portlet box blue" id="form_wizard_1">

        <div class="portlet-title">

            <div class="caption">

                <i class="icon-reorder"></i> 添加医院信息

            </div>

        </div>

        <div class="portlet-body form">

        <form action="hospital/doAdd" class="form-horizontal" id="submit_form">

        <div class="form-wizard">

        <div class="navbar steps">

            <div class="navbar-inner">

                <ul class="row-fluid">

                    <li class="span3">

                        <a href="#tab1" data-toggle="tab" class="step active">

                            <span class="number">第一步</span>

                            <span class="desc"><i class="icon-ok"></i>基本信息</span>

                        </a>

                    </li>

                    <li class="span3">

                        <a href="#tab2" data-toggle="tab" class="step">

                            <span class="number">第二步</span>

                            <span class="desc"><i class="icon-ok"></i>详细信息</span>

                        </a>

                    </li>

                    <li class="span3">

                        <a href="#tab3" data-toggle="tab" class="step">

                            <span class="number">第三步</span>

                            <span class="desc"><i class="icon-ok"></i>配置项目</span>

                        </a>

                    </li>

                    <li class="span3">

                        <a href="#tab4" data-toggle="tab" class="step">

                            <span class="number">第四步</span>

                            <span class="desc"><i class="icon-ok"></i>确认</span>

                        </a>

                    </li>

                </ul>

            </div>

        </div>

        <div id="bar" class="progress progress-success progress-striped">

            <div class="bar"></div>

        </div>

        <div class="tab-content">

        <div class="alert alert-error hide">

            <button class="close" data-dismiss="alert"></button>

            验证不通过,请查看如下信息.

        </div>

        <div class="alert alert-success hide">

            <button class="close" data-dismiss="alert"></button>

            验证通过!

        </div>

        <div class="tab-pane active" id="tab1">

            <h3 class="block">请输入医院基本信息</h3>

            <div class="control-group">

                <label class="control-label">医院名称<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="hospitalName"/>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院别名</label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="hospitalAlias" id="hospitalAlias"/>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院电话<span class="required">*</span></label>

                <div class="controls">

                    <input type="number" class="span6 m-wrap" name="phone"/>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">所在地区<span class="required">*</span></label>

                <div class="controls">

                    <select id="seachprov" name="province" class="span3" onchange="changeComplexProvince(this.value, sub_array, 'seachcity', 'seachdistrict');">
                    </select>
                    &nbsp;&nbsp;
                    <select id="seachcity" name="city" class="span3"  onchange="changeCity(this.value,'seachdistrict','seachdistrict');">
                    </select>
                    &nbsp;&nbsp;
                <span id="seachdistrict_div">
                    <select id="seachdistrict" name="seachdistrict">
                    </select>
                </span>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院地址<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="hospitalAddress"/>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院属性<span class="required">*</span></label>

                <div class="controls">

                    <select name="hospitalNature" id="hospitalList" class="span6">
                        <option value="1">公立医院</option>
                        <option value="2">私立医院</option>
                    </select>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">项目分级<span class="required">*</span></label>

                <div class="controls">

                    <select name="projectLevel" id="levelList" class="span6">
                        <option value="1">一级</option>
                        <option value="2">二级</option>
                        <option value="3">三级</option>
                    </select>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院网址<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="hospitalUrl" />

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">特色项目<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="featuresProject"/>

                </div>

            </div>

        </div>

        <div class="tab-pane" id="tab2">

            <h3 class="block">输入联系人信息</h3>

            <div class="control-group">

                <label class="control-label">姓名:<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="contactsName"  placeholder="请输入姓名"/>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">性别:<span class="required">*</span></label>

                <div class="controls">

                    <label class="radio">

                        <input type="radio" name="gender" value="1" checked />   女

                    </label>

                    <label class="radio">

                        <input type="radio" name="gender" value="0"  />男

                    </label>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">手机:<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="contactsPhone"  placeholder="请输入手机号"/>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">职位:<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="position"  placeholder="请输入职位"/>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">Email:<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="email"  placeholder="请输入邮箱地址"/>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">QQ:<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="qq"  placeholder="请输入QQ"/>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">微信:<span class="required">*</span></label>

                <div class="controls">

                    <input type="text" class="span6 m-wrap" name="wechat"  placeholder="请输入微信号"/>

                </div>

            </div>

        </div>
        <div class="tab-pane" id="tab3">

            <h3 class="block">请配置医院合约项目</h3>

            <div class="control-group">

                <label class="control-label">合约项目<span class="required">*</span></label>

                <div class="controls">
                    <div class="row-fluid">
                        <div class="span4 m-wrap">
                            <table class="table table-bordered table-hover" id="proTable">
                                <thead>
                                <tr>
                                    <th>项目名称</th>
                                    <th>设置提成比例(%)</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>双眼皮<input type="hidden" value="双眼皮" name="projectName[0]" ><input type="hidden" value="2" name="projectID[0]" ><input type="hidden" value="10000" name="indicativePrice[0]" ></td>
                                    <td><input type="text" value="" name="commission[0]" ></td>
                                    <td><span class="label label-success" onclick="deleteRow(this)">删除</span></td>
                                </tr>
                                <tr>
                                    <td>丰胸<input type="hidden" value="丰胸" name="projectName[1]" ><input type="hidden" value="3" name="projectID[1]" ><input type="hidden" value="50000" name="indicativePrice[1]" ></td>
                                    <td><input type="text" value="" name="commission[1]" ></td>
                                    <td><span class="label label-success" onclick="deleteRow(this)">删除</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="span4 m-wrap">
                            <select id="partlist" class="span7 m-wrap" onchange="changePart()">
                               <c:forEach var="part" items="${partArray}">
                                   <option value="${part.id}">${part.name}</option>
                               </c:forEach>
                            </select>
                            <select id="parlistDetail" class="span7 m-wrap" multiple="multiple" data-placeholder="选择项目" tabindex="1">
                                <c:forEach var="pro" items="${projectArray}">
                                   <option value="${pro.id}">${pro.projectName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="control-group">

                <label class="control-label">合同到期日期<span class="required">*</span></label>

                <div class="controls">

                    <div class="input-append date date-picker" data-date="2015-10-10" data-date-format="yyyy-mm-dd" data-date-viewmode="years">

                        <input class="m-wrap m-ctrl-medium date-picker" readonly size="16" type="text" name="contractDeadline" value="" /><span class="add-on"><i class="icon-calendar"></i></span>

                    </div>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">认证状态<span class="required">*</span></label>

                <div class="controls">

                    <label class="radio">

                        <input type="radio" name="status" value="1" checked /> 是

                    </label>

                    <label class="radio">

                        <input type="radio" name="status" value="0"  />否

                    </label>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">认证证明<span class="required">*</span></label>

                <div class="controls">

                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <div class="input-append">

                            <div class="uneditable-input">

                                <i class="icon-file fileupload-exists"></i>

                                <span class="fileupload-preview"></span>

                            </div>

                            <span class="btn btn-file">

                            <span class="fileupload-new">选择文件</span>

                            <span class="fileupload-exists">更换</span>

                            <input type="file" class="default" name="certification"/>

                            </span>

                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">清除</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="tab-pane" id="tab4">

            <h3 class="block">确认输入信息</h3>

            <h4 class="form-section">医院资料</h4>

            <div class="control-group">

                <label class="control-label">医院名称:</label>

                <div class="controls">

                    <span class="text display-value" data-display="hospitalName"></span>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院别名:</label>

                <div class="controls">

                    <span class="text display-value" data-display="hospitalAlias"></span>

                </div>

            </div>


            <div class="control-group">

                <label class="control-label">医院电话:</label>

                <div class="controls">

                    <span class="text display-value" data-display="phone"></span>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院地址:</label>

                <div class="controls">

                    <span class="text display-value" data-display="address"></span>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">医院属性:</label>

                <div class="controls">

                    <span class="text display-value" data-display="attribute"></span>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">项目分级:</label>

                <div class="controls">

                    <span class="text display-value" data-display="proLevel"></span>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">医院网址:</label>

                <div class="controls">

                    <span class="text display-value" data-display="hosptialUrl"></span>

                </div>

            </div>

            <div class="control-group">

                <label class="control-label">特色项目:</label>

                <div class="controls">

                    <span class="text display-value" data-display="fullname"></span>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">合同到期日期:</label>

                <div class="controls">

                    <span class="text display-value" data-display="fullname"></span>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">认证状态:</label>

                <div class="controls">

                    <span class="text display-value" data-display="fullname"></span>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">联系人姓名:</label>

                <div class="controls">

                    <span class="text display-value" data-display="fullname"></span>

                </div>

            </div>
            <div class="control-group">

                <label class="control-label">联系人电话:</label>

                <div class="controls">

                    <span class="text display-value" data-display="fullname"></span>

                </div>

            </div>

        </div>

        </div>

        <div class="form-actions clearfix">

            <a href="javascript:;" class="btn button-previous">

                <i class="m-icon-swapleft"></i>上一步

            </a>

            <a href="javascript:;" class="btn blue button-next">

                下一步 <i class="m-icon-swapright m-icon-white"></i>

            </a>

            <a href="javascript:;" class="btn green button-submit" onclick="doAddHospital();">

                提交 <i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

        </div>

        </form>

        </div>

        </div>

    </div>

</div>


<!--内容结束 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-fileupload.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/additional-methods.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/select2.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/date.js"></script>


<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>

<script src="<%=request.getContextPath()%>/script/hospital.js"></script>

<script src="<%=request.getContextPath()%>/script/Area.js"></script>

<script src="<%=request.getContextPath()%>/script/common.js"></script>

<script src="<%=request.getContextPath()%>/script/AreaData_min.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>

    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();

        FormWizard.init();

        initComplexArea('seachprov', 'seachcity', 'seachdistrict', area_array, sub_array, '44', '0', '0');

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl : App.isRTL(),
                format : "yyyy-mm-dd"
            });
        }

        $('#add_hospital_btn').click(function(){
            window.location = '<%=request.getContextPath()%>/hospital/toAdd';
        });
        $("#parlistDetail").live("click",function(){
            var objValue=$("#parlistDetail option:selected").html();
            var i=0;
            $("#proTable").find("tr").each(function(){
                var tempVal=$(this).children("td").eq(0).html();
                if(tempVal==objValue){
                    alert("此条项目已经被选择过了!");
                    i++;
                    return;
                }
            });
            if(i==0){
                $("#proTable").append("<tr><td>"+objValue+"</td><td></td><td><span class='label label-success' onclick='deleteRow(this)'>删除</span></td></tr>");
            }
        });
    });

    //得到地区码
    function  getAreaID() {
        var area = 0;
        if ($("#seachdistrict").val() != "0") {
            area = $("#seachdistrict").val();
        }
        else if ($("#seachcity").val() != "0") {
            area = $("#seachcity").val();
        }
        else {
            area = $("#seachprov").val();
        }

        return area;
    }

    function showAreaID() {
        //地区码
        var areaID = getAreaID();
        //地区名
        var areaName = getAreaNamebyID(areaID) ;
        alert("您选择的地区码：" + areaID + "      地区名：" + areaName);
    }

    //根据地区码查询地区名
    function getAreaNamebyID(areaID) {
        var areaName = "";
        if (areaID.length == 2) {
            areaName = area_array[areaID];
        }
        else if (areaID.length == 4) {
            var index1 = areaID.substring(0, 2);
            areaName = area_array[index1] + " " + sub_array[index1][areaID];
        }
        else if (areaID.length == 6) {
            var index1 = areaID.substring(0, 2);
            var index2 = areaID.substring(0, 4);
            areaName = area_array[index1] + " " + sub_array[index1][index2] + " " + sub_arr[index2][areaID];
        }
        return areaName;
    }
    function  changePart(){
         var partId=$("#partlist option:selected").val();
         var url="${ctx}/hospital/getPorjectListByPart?partId="+partId;
        $.ajax({
             type:"GET",
             url:url,
             dataType:"text",
             success:function(data){
                  if(data&&data.code=='SUCCESS'){
                     var pList= data.data;
                      $("#parlistDetail").empty();
                     for(var i=0;i<pList.length;i++){
                           $("#parlistDetail").append('<option value="'+pList[i].id+'">'+pList[i].projectName+'</option>');
                     }
                  }
             }
        });
    }

    function deleteRow(obj){
        $(obj).parent().parent().remove();
    }



    function doAddHospital(){

       getData("POST","${ctx}/hospital/addHospital",getFormData($("#submit_form")),null,"callbcakAddHospital");

    }
    function callbcakAddHospital(cb){
         alert(cb);
    }

</script>

</body>
</html>