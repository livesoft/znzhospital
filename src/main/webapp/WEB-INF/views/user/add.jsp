<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>客服信息管理</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/select2_metro.css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/chosen.css" />

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/datepicker.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/multi-select-metro.css">

    <!-- END PAGE LEVEL STYLES -->

</head>
<body >
<!--内容开始-->
<div class="row-fluid">

<div class="span12">

<!-- BEGIN SAMPLE FORM PORTLET-->

<div class="portlet box blue">

<div class="portlet-title">

    <div class="caption"><i class="icon-reorder"></i>添加客服</div>

</div>

<div class="portlet-body form">

<!-- BEGIN FORM-->

<form action="user/addUser" class="form-horizontal">

<div class="control-group">

    <label class="control-label">客服名称</label>

    <div class="controls">

        <input type="text" class="span6 m-wrap" placeholder="请输入客服名称" >

    </div>

</div>

<div class="control-group">

    <label class="control-label">用户名</label>

    <div class="controls">

        <input class="span6 m-wrap" type="text" placeholder="请输入用户名" >


    </div>

</div>

<div class="control-group">

    <label class="control-label">联系电话</label>

    <div class="controls">

        <input class="span6 m-wrap" type="text" placeholder="输入联系电话" >

    </div>

</div>

<div class="control-group">

        <label class="control-label">设置提成</label>

    <div class="controls">

        <input type="text" class="span6 m-wrap popovers"  placeholder="输入提成" >

    </div>

</div>

<div class="control-group">

    <label class="control-label">客服等级</label>

    <div class="controls">

        <select class="span6 m-wrap" data-placeholder="客服等级" tabindex="1">

            <option value="">请选择...</option>

            <option value="1">一级</option>

            <option value="2">二级</option>

            <option value="3">三级</option>

        </select>

    </div>

</div>

<div class="control-group">

    <label class="control-label">擅长项目<span class="required">*</span></label>

    <div class="controls">
        <div class="row-fluid">
            <div class="span4 m-wrap">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>项目名称</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>双眼皮</td>
                        <td><span class="label label-success">删除</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="span4 m-wrap">
                <select id="partlist" class="span7 m-wrap">
                    <option value="1">眼睛</option>
                    <option value="2">鼻子</option>
                    <option value="3">胸部</option>
                </select>

                <select class="span7 m-wrap" multiple="multiple" data-placeholder="选择项目" tabindex="1">
                    <option value="Category 1">双眼皮</option>
                    <option value="Category 2">文眉</option>
                    <option value="Category 3">开眼角</option>
                    <option value="Category 4">去眼袋</option>
                </select>
            </div>
        </div>
    </div>

</div>

<div class="form-actions">

    <button type="button" class="btn blue">提交</button>

    <button type="button" class="btn">取消</button>

</div>

</form>

<!-- END FORM-->

</div>

</div>

<!-- END SAMPLE FORM PORTLET-->

</div>

</div>

<!--内容结束 -->


<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-fileupload.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/additional-methods.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/select2.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/date.js"></script>


<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>

<script src="<%=request.getContextPath()%>/script/hospital.js"></script>

<script src="<%=request.getContextPath()%>/script/Area.js"></script>

<script src="<%=request.getContextPath()%>/script/AreaData_min.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>

    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();

        FormWizard.init();

        initComplexArea('seachprov', 'seachcity', 'seachdistrict', area_array, sub_array, '44', '0', '0');

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl : App.isRTL(),
                format : "yyyy-mm-dd"
            });
        }

        $('#add_hospital_btn').click(function(){
            window.location = '<%=request.getContextPath()%>/hospital/toAdd';
        });
    });

    //得到地区码
    function  getAreaID() {
        var area = 0;
        if ($("#seachdistrict").val() != "0") {
            area = $("#seachdistrict").val();
        }
        else if ($("#seachcity").val() != "0") {
            area = $("#seachcity").val();
        }
        else {
            area = $("#seachprov").val();
        }

        return area;
    }

    function showAreaID() {
        //地区码
        var areaID = getAreaID();
        //地区名
        var areaName = getAreaNamebyID(areaID) ;
        alert("您选择的地区码：" + areaID + "      地区名：" + areaName);
    }

    //根据地区码查询地区名
    function getAreaNamebyID(areaID) {
        var areaName = "";
        if (areaID.length == 2) {
            areaName = area_array[areaID];
        }
        else if (areaID.length == 4) {
            var index1 = areaID.substring(0, 2);
            areaName = area_array[index1] + " " + sub_array[index1][areaID];
        }
        else if (areaID.length == 6) {
            var index1 = areaID.substring(0, 2);
            var index2 = areaID.substring(0, 4);
            areaName = area_array[index1] + " " + sub_array[index1][index2] + " " + sub_arr[index2][areaID];
        }
        return areaName;
    }

</script>

</body>
</html>