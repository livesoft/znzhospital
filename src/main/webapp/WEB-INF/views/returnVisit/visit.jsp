<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>回访记录</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/select2_metro.css" />

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/chosen.css" />

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/datepicker.css" rel="stylesheet" type="text/css"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/profile.css" rel="stylesheet" type="text/css"/>

    <link href="<%=request.getContextPath()%>/themes/bootstrap/css/timeline.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/themes/bootstrap/css/multi-select-metro.css">

    <!-- END PAGE LEVEL STYLES -->

</head>
<body >
<!--内容开始 -->

<div class="row-fluid profile">

<div class="span12">

<!--BEGIN TABS-->

<div class="tabbable tabbable-custom tabbable-full-width">

<ul class="nav nav-tabs">

    <li class="active"><a href="#tab_1_1" data-toggle="tab">顾客信息</a></li>

    <li><a href="#tab_1_2" data-toggle="tab">历史回访</a></li>

    <li><a href="#tab_1_3" data-toggle="tab">设置回访</a></li>

</ul>

<div class="tab-content">

<div class="tab-pane profile-classic active row-fluid" id="tab_1_1">

    <div class="span2">
        <%-- <img src="media/image/profile-img.png" alt="" />
         <a href="#" class="profile-edit">edit</a>--%>
    </div>

    <ul class="unstyled span10">

        <li><span>订单号:</span>HL28131341</li>

        <li><span>姓名:</span>王美丽</li>

        <li><span>联系电话:</span>13621059717</li>

        <li><span>所在地区:</span>北京市，海淀区</li>

        <li><span>需求意向:</span>隆鼻，隆胸，提臀，美肤</li>

    </ul>
    <div class="tabbable tabbable-custom tabbable-custom-profile">

        <ul class="nav nav-tabs">

            <li class="active"><a href="#tab_1_11" data-toggle="tab">派发到的医院</a></li>

        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="tab_1_11">

                <div class="portlet-body" style="display: block;">

                    <table class="table table-striped table-bordered table-advance table-hover">

                        <thead>

                        <tr>

                            <th><i class="icon-briefcase"></i>医院名称</th>

                            <th class="hidden-phone"><i class="icon-question-sign"></i>联系电话</th>

                            <th><i class="icon-bookmark"></i>状态</th>

                            <th>操作</th>

                        </tr>

                        </thead>

                        <tbody>

                        <tr>

                            <td><a href="#">北京东方瑞丽</a></td>

                            <td class="hidden-phone">13621059717</td>

                            <td><span class="label label-success label-mini">未回访</span></td>

                            <td><a class="btn mini green-stripe" href="#">查看</a></td>

                        </tr>

                        </tbody>

                    </table>

                </div>

            </div>

        <!--tab-pane-->

        </div>

    </div>

    </div>

<!--tab_1_2-->

<div class="tab-pane" id="tab_1_2">

<div class="row-fluid">

<div class="span12">

<ul class="timeline">

<li class="timeline-yellow">

    <div class="timeline-time">

        <span class="date">2015-01-05</span>

        <span class="time">18:30</span>

    </div>

    <div class="timeline-icon"><i class="icon-trophy"></i></div>

    <div class="timeline-body">

        <h2>接单回访</h2>

        <div class="timeline-content">

            客户反馈对东方瑞丽比较感兴趣，准备3月1号面诊。

        </div>

        <div class="timeline-footer">

            <a href="#" class="nav-link pull-right">

                修改<i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

    </div>

</li>

<li class="timeline-blue">

    <div class="timeline-time">

        <span class="date">2015-03-05</span>

        <span class="time">12:04</span>

    </div>

    <div class="timeline-icon"><i class="icon-facetime-video"></i></div>

    <div class="timeline-body">

        <h2>到院回访</h2>

        <div class="timeline-content">

            洽谈成功，3月10日做。

        </div>

        <div class="timeline-footer">

            <a href="#" class="nav-link">

                修改<i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

    </div>

</li>

<li class="timeline-green">

    <div class="timeline-time">

        <span class="date">2015-03-08</span>

        <span class="time">05:36</span>

    </div>

    <div class="timeline-icon"><i class="icon-comments"></i></div>

    <div class="timeline-body">

        <h2>结单回访</h2>

        <div class="timeline-content">

             手术完成，费用13000。

        </div>

        <div class="timeline-footer">

            <a href="#" class="nav-link">

                修改<i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

    </div>

</li>

<li class="timeline-purple">

    <div class="timeline-time">

        <span class="date">2015-03-12</span>

        <span class="time">13:15</span>

    </div>

    <div class="timeline-icon"><i class="icon-music"></i></div>

    <div class="timeline-body">

        <h2>二次开发</h2>

        <div class="timeline-content">

                准备7月份做鼻子。

        </div>

        <div class="timeline-footer">

            <a href="#" class="nav-link">

                修改<i class="m-icon-swapright m-icon-white"></i>

            </a>

        </div>

    </div>

</li>

</ul>

</div>

</div>

</div>

<!--end tab-pane-->

<div class="tab-pane row-fluid profile-account" id="tab_1_3">

<div class="row-fluid">

<div class="span12">

<div class="tab-content">

    <div id="tab_1-1" class="tab-pane active">

        <div style="height: auto;" id="accordion1-1" class="accordion collapse">

            <form action="#">

                <label class="control-label">本次回访状态</label>

                <select class="span6 m-wrap" data-placeholder="回访状态" tabindex="1">

                    <option value="">请选择...</option>

                    <option value="1">接单回访</option>

                    <option value="2">到院回访</option>

                    <option value="3">结单回访</option>

                    <option value="4">退单回访</option>

                    <option value="5">二次开发</option>

                    <option value="6">其它</option>

                </select>

                <label class="control-label">本次回访内容</label>

                <textarea class="span8 m-wrap" rows="3"></textarea>

                <label class="control-label">下次回访时间</label>

                <div class="input-append date date-picker" data-date="2015-10-10" data-date-format="yyyy-mm-dd" data-date-viewmode="years">

                    <input class="m-wrap m-ctrl-medium date-picker" readonly size="16" type="text" value="" /><span class="add-on"><i class="icon-calendar"></i></span>

                </div>

                <div class="submit-btn">

                    <a href="#" class="btn green">保存</a>

                    <a href="#" class="btn">取消</a>

                </div>

            </form>

        </div>

    </div>

</div>

<!--end span9-->

</div>

</div>

</div>

</div>

</div>

<!--END TABS-->

</div>

</div>

<!--内容结束 -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-fileupload.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/additional-methods.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/select2.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/themes/bootstrap/js/date.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<%=request.getContextPath()%>/themes/bootstrap/js/app.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>

    jQuery(document).ready(function() {

        App.init();

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl : App.isRTL(),
                format : "yyyy-mm-dd"
            });
        }

    });
</script>

</body>
</html>