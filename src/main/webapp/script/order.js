var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            $("#country_list").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            var form = $('#submit_form');
            var error = $('.alert-error', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'validate-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    username: {
                        minlength: 2,
                        required: true
                    },
                    tel: {
                        minlength: 11,
                        required: true,
                        number: true,
                        maxlength: 18
                    },
                    age:{
                        required: true,
                        number: true,
                        minlength:1,
                        maxlength:2
                    },
                    'payment[]': {
                        required: true,
                        minlength: 1
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.format("Please select at least one option")
                    },
                    tel:{
                        required:"请输入手机号",
                        number:"必须为数字",
                        minlength:"联系电话必须为11位",
                        maxlength:"联系电话不得大于18位"
                    }

                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.addClass("no-left-padding").insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.addClass("no-left-padding").insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavoir
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radip buttons, no need to show OK icon
                        label
                            .closest('.control-group').removeClass('error').addClass('success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid ok') // mark the current input as valid and display OK icon
                            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('.display-value', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":text") || input.is("textarea")|| input.attr("type")=="number") {
                        $(this).html(input.val());
                    }else if($(this).attr("data-display") == 'sex'){
                        var sex=$("input[name='sex']:checked").val();
                        if(sex==1){
                            $(this).html('女');
                        }else if(sex==0){
                            $(this).html('男');
                        }
                    }else if ($(this).attr("data-display") == 'address') {
                        var address = [];
                        $('[name^="address"]').each(function(){
                            address.push($(this).find("option:selected").text());
                        });
                        $(this).html(address.join("-->"));
                    }else if($(this).attr("data-display") == 'proTable'){
                        var projects = [];
                        $("#proTable>tbody>tr").each(function(){
                            projects.push($(this).children("td").first().text());
                        });
                        $(this).html(projects.join("、"));
                    }else if($(this).attr("data-display") == 'hospitalTable'){
                        var hospitals = [];
                        $("#hospitalTable>tbody>tr").each(function(){
                            hospitals.push($(this).children("td").first().text());
                        });
                        $(this).html(hospitals.join("、"));
                    }
                });
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index) {
                    alert('on tab click disabled');
                    return false;
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form_wizard_1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form_wizard_1').find('.button-previous').hide();
                    } else {
                        $('#form_wizard_1').find('.button-previous').show();
                    }
                    if (current == 2) {
                        $('#form_wizard_1').find('.button-next').html("保存订单并分配医院");
                    }else{
                        $('#form_wizard_1').find('.button-next').html("下一步");
                    }
                    if(current == 3){
                        var order=getOrderParam();
                        console.info($("#submit_form").serialize());
                        console.info($("#submit_form").serializeArray());
                        console.info(JSON.stringify($("#submit_form").serializeArray()));
                        getData("POST","saveOrder",order,null,null,"callbcakSaveOrder");
                    }

                    if (current >= total) {
                        $('#form_wizard_1').find('.button-next').hide();
                        $('#form_wizard_1').find('.button-submit').show();
                        displayConfirm();
                    } else {
                        $('#form_wizard_1').find('.button-next').show();
                        $('#form_wizard_1').find('.button-submit').hide();
                    }
                    App.scrollTo($('.page-title'));
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form_wizard_1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form_wizard_1').find('.button-previous').hide();
                    } else {
                        $('#form_wizard_1').find('.button-previous').show();
                    }
                    if (current == 2) {
                        $('#form_wizard_1').find('.button-next').html("保存订单并分配医院");
                    }else{
                        $('#form_wizard_1').find('.button-next').html("下一步");
                    }

                    if (current >= total) {
                        $('#form_wizard_1').find('.button-next').hide();
                        $('#form_wizard_1').find('.button-submit').show();
                    } else {
                        $('#form_wizard_1').find('.button-next').show();
                        $('#form_wizard_1').find('.button-submit').hide();
                    }
                    App.scrollTo($('.page-title'));
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                alert('Finished! Hope you like it :)');
            }).hide();
        }

    };

}();

function getOrderParam(){
    var userName=$("#username").val();
    var userAge=$("#age").val();
    var userPhone=$("#tel").val();
    var userCity=$("#seachdistrict option:selected").val();
    var userSex=$('input:radio[name=sex]:checked').val();
    var note =$("#note").val();
    var userMessage=$("#userMessage").val();

    return "userName="+userName+"&userAge="+userAge
        +"&userPhone="+userPhone+"&userCity="+userCity
        +"&userSex="+userSex+"&note="+note+"&userMessage="+userMessage;
}

function callbcakSaveOrder(cb){
    console.info(cb);
}