function getData(ajaxtype,url,param,beforeSendF,beforeSendFData,callback){
    $.ajax({
        type:ajaxtype,
        url:url,
        data:param,
        dataType:'json',
        contentType:"application/json",
        beforeSend:function()
        {
            if(beforeSendF!=null&&beforeSendFData!=null){
                window[beforeSendF](beforeSendFData);
            }else if(beforeSendF!=null){
                window[beforeSendF]();
            }
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
            alert("网络错误");
        },
        success:function(data)
        {
            if(data.code == 'SUCCESS')
            {
                if(callback!=null){
                    window[callback](data.data);
                }
            }
            else
            {
                alert(data.msg);
            }
        }
    });
}

function getFormData($form){
    var serialized = $form.serializeArray();
    var s = '';
    var data = {};
    for(s in serialized){
        data[serialized[s]['name']] = serialized[s]['value']
    }
    return JSON.stringify(data);
}