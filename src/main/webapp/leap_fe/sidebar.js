if(r_list){
    buildSystemSidebar(r_list);
}

function buildSystemSidebar(r_list){

    var c_url =window.location.pathname;
    var  n_temp="";
    for(var m=0;m<r_list.length;m++){
        var r_m = r_list[m];
        if(r_m.resourceLevel.id==1){   //nav each 是否一级菜单
            var n_li=$("<li class=''></li>");
            var n_a="<a href='javascript:;'><i class='icon-cogs'></i><span class='title'>"+r_m.resourceName+"</span> <span class='arrow '></span></a>";
            n_li.append(n_a);
            var nc_ul=$("<ul class='sub-menu'></ul>");
            for(var s=0;s<r_list.length;s++){  // nav items each
                var r_s=r_list[s];
                if(r_s.parentId.id!=''&&r_s.parentId.id==r_m.id){  //判断是否是二级菜单
                    if(c_url.indexOf(r_s.resourceUrl)!=-1){
                        n_li.addClass("open");
                        n_li.find(".arrow").addClass("open");
                        nc_ul.attr("style","display: block;");
                        nc_ul.append("<li><a href='"+contextPath+r_s.resourceUrl+"'>"+r_s.resourceName+"</a></li>");
                        continue;
                    }
                    nc_ul.append("<li><a href='"+contextPath+r_s.resourceUrl+"'>"+r_s.resourceName+"</a></li>");
                }
            }
            nc_ul.appendTo(n_li);
            n_temp+=n_li.prop("outerHTML");
        }
    }
    $("#navbar").append(n_temp);
}
