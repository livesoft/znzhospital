package com.znz.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("/user")
@Controller
public class UserController extends AbstractController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);
    /**
     * 菜单整形项目界面
     * @return
     */
    @RequestMapping(value={"/","/user"})
    public ModelAndView userList() {
        ModelAndView modelAndView=null;
        modelAndView = getTemplate("user/list",null);
        return modelAndView;
    }

    /**
     * 本方法是新增一个客服账户
     * @return
     */
    @RequestMapping(value="toAdd")
    private ModelAndView toAdd() {
        try {
            //初始化下拉菜单数据
            Map<String,Object> map=new HashMap<String,Object>();
            //TODO 需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView = getTemplate("user/add",map);

            return modelAndView;
        } catch (Exception e) {
            logger.error("新增客服页面初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }

    }

    @RequestMapping(value="toEdit")
    private ModelAndView toEdit(Long project_id) {
        try {

            //TODO 这里需要根据客服id获取客服对象放到map里

            Map<String,Object> map=new HashMap<String,Object>();

            //TODO 同时还需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView = getTemplate("system/project/save",map);

            return modelAndView;
        }catch (Exception e){
            logger.error("修改客服页面初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }
    }

}
