package com.znz.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;



@RequestMapping("/demand")
@Controller
public class DemandController extends AbstractController {

    private Logger logger = LoggerFactory.getLogger(DemandController.class);
    /**
     * m美丽需求列表页面根据当前客服id查询，如果为超级管理员则查询出所有
     * @return
     */
    @RequestMapping(value={"/","/demand"})
    public ModelAndView demandList() {
        ModelAndView modelAndView=null;
        modelAndView = getTemplate("demand/list",null);
        return modelAndView;
    }

    /**
     * 本方法是新增一个医院账户
     * @return
     */
    @RequestMapping(value="toAdd")
    private ModelAndView toAdd() {
        try {
            //初始化下拉菜单数据
            Map<String,Object> map=new HashMap<String,Object>();
            //TODO 需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView = getTemplate("demand/add",map);

            return modelAndView;
        } catch (Exception e) {
            logger.error("新增医院页面初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }

    }

    @RequestMapping(value="toEdit")
    private ModelAndView toEdit(Long demand_id) {
        try {

            //TODO 这里需要根据医院id获取医院对象放到map里

            Map<String,Object> map=new HashMap<String,Object>();

            //TODO 同时还需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView = getTemplate("system/project/save",map);

            return modelAndView;
        }catch (Exception e){
            logger.error("修改医院页面初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }
    }

    @RequestMapping(value="toCheck")
    private ModelAndView toCheck(Long demand_id) {
        try {

            //TODO 这里需要根据医院id获取医院对象放到map里

            Map<String,Object> map=new HashMap<String,Object>();

            //TODO 同时还需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView = getTemplate("demand/check",map);

            return modelAndView;
        }catch (Exception e){
            logger.error("修改医院页面初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }
    }

}
