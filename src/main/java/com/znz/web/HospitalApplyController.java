package com.znz.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("/apply")
@Controller
public class HospitalApplyController extends AbstractController {

    private Logger logger = LoggerFactory.getLogger(HospitalApplyController.class);
    /**
     * 医院注册页面
     * @return
     */
    @RequestMapping(value={"/","/apply"})
    public ModelAndView toApply() {
        try {
            //初始化下拉菜单数据
            Map<String,Object> map=new HashMap<String,Object>();
            //TODO 需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView=null;
            modelAndView = getTemplate("apply/apply",null);
            return modelAndView;
        } catch (Exception e) {
            logger.error("新增医院页面初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }
    }
}
