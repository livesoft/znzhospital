package com.znz.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 对回访信息进行操作
 */

@RequestMapping("/returnvisit")
@Controller
public class ReturnVisitController extends AbstractController {

    private Logger logger = LoggerFactory.getLogger(ReturnVisitController.class);
    /**
     * h回访记录列表页面根据当前客服id查询，如果为超级管理员则查询出所有
     * @return
     */
    @RequestMapping(value={"/","/returnvisit"})
    public ModelAndView demandList() {
        ModelAndView modelAndView=null;
        modelAndView = getTemplate("returnVisit/list",null);
        return modelAndView;
    }

    @RequestMapping(value="visit")
    private ModelAndView toEdit(Long visit_id) {
        try {

            //TODO 这里需要根据医院id获取医院对象放到map里

            Map<String,Object> map=new HashMap<String,Object>();

            //TODO 同时还需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView = getTemplate("returnVisit/visit",map);

            return modelAndView;
        }catch (Exception e){
            logger.error("修改医院页面初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }
    }

}
