package com.znz.web;

import com.znz.service.OrderService;
import com.znz.util.common.ReturnObj;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * OrderController
 * 订单Controller
 * @author donglin Ni
 * @date 2015/4/26
 */
@RequestMapping("/order")
@Controller
public class OrderController extends AbstractController{

    @Autowired
    private OrderService orderService;

    private Logger logger = LoggerFactory.getLogger(OrderController.class);
    /**
     * 客服查看订单列表页
     * @return
     */
    @RequestMapping(value={"/","/list"})
    public ModelAndView orderList() {
        ModelAndView modelAndView=null;
        modelAndView = getTemplate("order/list",null);
        return modelAndView;
    }
    /**
     * 医院查看订单列表页
     * @return
     */
    @RequestMapping(value="/hospital/list")
    public ModelAndView orderListForHospital() {
        ModelAndView modelAndView=null;
        modelAndView = getTemplate("order/hospital/list",null);
        return modelAndView;
    }
    /**
     * 新增订单
     * @return
     */
    @RequestMapping(value="/toAdd")
    private ModelAndView toAdd() {
        try {
            //初始化下拉菜单数据
            Map<String,Object> map=new HashMap<String,Object>();
            //TODO 获取所有的部位
            JSONArray partArray=orderService.getPartAll();
            map.put("partArray",partArray);
            //TODO 根据部位ID获取所有项目
            JSONArray projectArray=orderService.getProjectByPartId(1);
            map.put("projectArray",projectArray);
            //TODO 根据省份ID和城市ID获取医院列表
            JSONArray hospitalArray=orderService.getHospitalAllByCityId(1,1,null);
            map.put("hospitalArray",hospitalArray);
            //TODO 需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView = getTemplate("order/add",map);
            return modelAndView;
        } catch (Exception e) {
            logger.error("新增订单初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }

    }

    /**
     * 订单详情
     * @return
     */
    @RequestMapping(value="/hospital/detail")
    private ModelAndView detail() {
        try {
            //初始化下拉菜单数据
            Map<String,Object> map=new HashMap<String,Object>();
            //TODO 需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView = getTemplate("order/hospital/detail",map);

            return modelAndView;
        } catch (Exception e) {
            logger.error("订单详情初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }

    }

    /**
     * 修改订单
     * @param orderid
     * @return
     */
    @RequestMapping(value="/toEdit")
    private ModelAndView toEdit(Long orderid) {
        try {

            //TODO 这里需要根据医院id获取医院对象放到map里

            Map<String,Object> map=new HashMap<String,Object>();

            //TODO 同时还需要获取项目列表放到map里，用于配置项目列表
            ModelAndView modelAndView = getTemplate("order/edit",map);

            return modelAndView;
        }catch (Exception e){
            logger.error("修改订单页面初始化异常",e);
            ModelAndView modelAndView = getTemplate("uncaughtException",null);
            return  modelAndView;
        }
    }

    @RequestMapping(value = "/getProvinceAllByParentId")
    @ResponseBody
    public ReturnObj  getProvinceAllByParentId(@RequestParam int parentId){
        logger.info("/order/getProvinceAllByParentId?parentId="+parentId);
        ReturnObj returnObj= null;
        //TODO 根据上级省市ID获取下级所有省市级数据
        JSONArray jsonArray=orderService.getProjectByPartId(parentId);
        returnObj=ReturnObj.createSuccess(jsonArray);
        return returnObj;
    }

    @RequestMapping(value = "/getPartAll")
    @ResponseBody
    public ReturnObj getPartAll(){
        logger.info("/order/getPartAll");
        ReturnObj returnObj=null;
        //TODO  获取所有的部位
        JSONArray jsonArray=orderService.getPartAll();
        returnObj=ReturnObj.createSuccess(jsonArray);
        return returnObj;
    }

    @RequestMapping(value = "/getProjectByPartId")
    @ResponseBody
    public ReturnObj getProjectByPartId(@RequestParam int partId){
        logger.info("/order/getProjectByPartId?partId="+partId);
        ReturnObj returnObj=null;
        //TODO 根据部位获取该部位所包含的项目接口
        JSONArray jsonArray=orderService.getProjectByPartId(partId);
        returnObj=ReturnObj.createSuccess(jsonArray);
        return returnObj;
    }

    @RequestMapping(value = "/saveOrder")
    @ResponseBody
    public ReturnObj saveOrder(Object order){
         ReturnObj returnObj=null;
        //TODO 保存订单信息
        JSONObject jsonObject=orderService.saveOrder();
        returnObj=ReturnObj.createSuccess(jsonObject);
        return   returnObj;
    }

    @RequestMapping(value = "/getHospitalAllByCityId")
    @ResponseBody
    public ReturnObj getHospitalAllByCityId(@RequestParam int cityId,@RequestParam int provinceid,@RequestParam String hospitalName){
        ReturnObj returnObj=null;
        //TODO 根据省份代码或者根据市区代码获取其下所有的医院
        logger.info("/order/getHospitalAllByCityId?cityId="+cityId);
        JSONArray jsonArray=orderService.getHospitalAllByCityId(cityId,provinceid,hospitalName);
        returnObj=ReturnObj.createSuccess(jsonArray);
        return returnObj;
    }

    @RequestMapping(value = "/getHospitalByLikeName")
    @ResponseBody
    public ReturnObj getHospitalByLikeName(@RequestParam String hospitalName){
         ReturnObj returnObj=null;
        //TODO 根据医院名称模糊查询医院
        logger.info("/order/getHospitalByLikeName?hospitalName="+hospitalName);
        JSONArray jsonArray=orderService.getHospitalByLikeName(hospitalName);
        returnObj=ReturnObj.createSuccess(jsonArray);
        return returnObj;
    }
    @RequestMapping(value = "/getHospitalAllByOrderId")
    @ResponseBody
    public  ReturnObj getHospitalAllByOrderId(@RequestParam int orderId){
       ReturnObj returnObj=null;
       //TODO 获取订单分配所有的医院
       logger.info("/order/getHospitalAllByOrderId?orderId="+orderId);
       JSONArray jsonArray=orderService.getHospitalAllByOrderId(orderId);
       returnObj=ReturnObj.createSuccess(jsonArray);
       return returnObj;
   }
    @RequestMapping(value = "/saveHospitalProject")
    @ResponseBody
    public  ReturnObj saveHospitalProject(@RequestParam int orderId,@RequestParam int hospitalProjectId,@RequestParam int expectCostMin,@RequestParam int expectCostMax){
       ReturnObj returnObj=null;
       //TODO 保存订单分配的医院
       logger.info("/order/saveHospitalProject?orderId="+orderId+"&hospitalProjectId="+hospitalProjectId+"&expectCostMin="+expectCostMin+"&expectCostMax="+expectCostMax);
       JSONObject jsonObject=orderService.saveHospitalProject(orderId,hospitalProjectId,expectCostMin,expectCostMax);
       returnObj=ReturnObj.createSuccess(jsonObject);
       return  returnObj;
   }
}

