package com.znz.web;

import com.znz.service.DashboardService;
import com.znz.util.common.ReturnObj;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created with IntelliJ IDEA.
 * User: duyuxin
 * Date: 15-4-29
 * Time: 下午11:34
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/dashboard")
public class DashboardController extends AbstractController {
    @Autowired
    private DashboardService homePageService;

    private Logger logger = LoggerFactory.getLogger(DashboardController.class);


    @RequestMapping(value={"/","/index"})
    public ModelAndView userList() {
        ModelAndView modelAndView=null;
        modelAndView = getTemplate("index",null);
        return modelAndView;
    }

    @RequestMapping(value = "/getAllOrderList")
    @ResponseBody
    public ReturnObj  getAllOrderList(@RequestParam String userPhone,@RequestParam String userId){
         ReturnObj returnObj=null;
        //TODO 查看当前客服的所有订单信息，如果是管理员则查看所有订单信息
        logger.info("/homePage/getAllOrderList?userPhone="+userPhone+"&userId="+userId);
        JSONArray jsonArray=homePageService.getAllOrderList(userPhone,userId);
        returnObj=ReturnObj.createSuccess(jsonArray);
        return returnObj;
    }

    @RequestMapping(value = "/getAchievement")
    @ResponseBody
    public ReturnObj getAchievement(@RequestParam String userId){
        ReturnObj returnObj=null;
        //TODO 根据当前客服id获取当月（自然月）业绩总额，如果是超级管理员则全部客服的业绩总额
        logger.info("/homePage/getAchievement?userId="+userId);
        JSONObject jsonObject=homePageService.getAchievement(userId);
        returnObj=ReturnObj.createSuccess(jsonObject);
        return  returnObj;
    }
    @RequestMapping(value = "/getDesireCount")
    @ResponseBody
    public  ReturnObj getDesireCount(@RequestParam String userId){
        ReturnObj returnObj=null;
        //TODO 根据当前客服id获取分配给自己的美丽需求数量
        logger.info("/homePage/getDesireCount?userId="+userId);
        JSONObject jsonObject=homePageService.getDesireCount(userId);
        returnObj=ReturnObj.createSuccess(jsonObject);
        return returnObj;
    }
    @RequestMapping(value = "/getReturnVisitCount")
    @ResponseBody
    public ReturnObj getReturnVisitCount(@RequestParam String userId){
        ReturnObj returnObj=null;
        //TODO 根据当前客服id获取当天待回访的数量
        logger.info("/homePage/getReturnVisitCount?userId="+userId);
        JSONObject jsonObject=homePageService.getReturnVisitCount(userId);
        returnObj=ReturnObj.createSuccess(jsonObject);
        return returnObj;
    }
}
