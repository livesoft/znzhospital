package com.znz.web;

import com.znz.domain.DictionaryDetail;
import com.znz.util.cache.CacheService;
import com.znz.util.constants.Constants;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * 系统资源管理
 * User: zhangshouchen
 * Date: 15-3-10
 * Time: 下午8:43
 */
@Controller
public abstract class AbstractController implements ApplicationContextAware {

    @Autowired
    protected CacheService cacheService;


	private ApplicationContext context;
    private static final  String   VERSION_NO_COPYRIGHT = ""; //TODO copyright   这个要换成配置文件

	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.context = applicationContext;
	}

    /**
     * common template method
     * @param templateName 对应的jsp路径
     * @param model 界面传递参数
     * @return
     */
        public ModelAndView getTemplate(String templateName,Map<String, Object> model) {
		ModelAndView modelView = new ModelAndView(templateName,model);
		ModelMap map = modelView.getModelMap();
		map.addAttribute("copyright",VERSION_NO_COPYRIGHT);
        //这里需要读取缓存中的用户信息和菜单资源信息
        map.addAttribute("userResourceList", getResourcesCache("123"));
		return modelView;
	}

    public JSONArray getResourcesCache(String username){
        //TODO 这里报错JSONObject循环嵌套错误
        JSONArray jsonObject = null;
        try {
            jsonObject = cacheService.get(username+"_hospitalResourceList");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return   jsonObject;
    }

    /**
     * 资源类型字典信息，先读取缓存中的，如果缓存没有则从数据库中读取
     * @param resourceCode 字典编号
     * @return
     * @throws Exception
     */
    protected List<DictionaryDetail> getDictionaries(String resourceCode) throws Exception {
        List<DictionaryDetail> dictionaryDetails =getCacheDictionaries(resourceCode);
       /* if(dictionaryDetails==null){
            dictionaryDetails =  dictionaryService.getDictionaryDetailsByName(resourceCode);
            this.cacheService.set(resourceCode,600,dictionaryDetails);
        }*/
        return dictionaryDetails;
    }

    /**
     *获取缓存中的字典信息
     *如果指定参数名则取出参数名对应的字典，如果为null则取出缓存的全部字典信息
     * @param dictionaryName
     * @return
     */
    protected  List<DictionaryDetail> getCacheDictionaries(String dictionaryName){
        //如果未指定字典名称则取出所有的字典列表
       /*if(dictionaryName!=null){
           if(cacheService.get(dictionaryName)!=null){
               return cacheService.get(dictionaryName);
           }else{
               return null;
           }
       }else{
           return cacheService.get(Constants.ALL_DICTIONARYS);
       }*/
             return null;
    }



	/**
	 * 获取登录后用户的基本信息
	 * @return
	 */
	public JSONObject getUserDetail() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("address","中国马甸桥西好医生办事处");
		jsonObject.put("mails","zhangjichao_12@163.com");
		jsonObject.put("mobile","13552980785");
		jsonObject.put("older","17");
		jsonObject.put("realName","天行者");
		jsonObject.put("","天行者");
		return jsonObject;

	}

}
