package com.znz.web;

import com.znz.domain.Thospital;
import com.znz.service.HospitalService;
import com.znz.service.OrderService;
import com.znz.util.common.Page;
import com.znz.util.common.ResponseJsonUtil;
import com.znz.util.common.ReturnObj;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.TypedQuery;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/hospital")
@Controller
public class HospitalController extends AbstractController {

	@Autowired
	private HospitalService hospitalService;

	@Autowired
	private OrderService orderService;

	private Logger logger = LoggerFactory.getLogger(HospitalController.class);

	/**
	 * 菜单整形项目界面
	 *
	 * @return
	 */
	@RequestMapping(value = {"/", "/hospital"})
	public ModelAndView hospitalList(String hospitaName) {
		try {
			ModelAndView modelAndView = null;
			Map<String, Object> map = new HashMap<String, Object>();
			JSONArray hospitalArray = hospitalService.getHospitalAll(hospitaName, 1);
			map.put("hospitalArray", hospitalArray);
			modelAndView = getTemplate("hospital/list", map);
			return modelAndView;
		} catch (Exception e) {
			logger.error("新增医院页面初始化异常", e);
			ModelAndView modelAndView = getTemplate("uncaughtException", null);
			return modelAndView;
		}
	}






	/**
	 * 本方法是新增一个医院账户
	 *
	 * @return
	 */
	@RequestMapping(value = "toAdd")
	private ModelAndView toAdd() {
		try {
			//初始化下拉菜单数据
			Map<String, Object> map = new HashMap<String, Object>();
			//TODO 获取所有部位
			JSONArray partArray = orderService.getPartAll();
			map.put("partArray", partArray);
			//TODO 根据部位ID获取项目列表
			JSONArray projectArray = hospitalService.getProjectListByPart("1");
			map.put("projectArray", projectArray);
			ModelAndView modelAndView = getTemplate("hospital/add", map);

			return modelAndView;
		} catch (Exception e) {
			logger.error("新增医院页面初始化异常", e);
			ModelAndView modelAndView = getTemplate("uncaughtException", null);
			return modelAndView;
		}

	}

	@RequestMapping(value = "toEdit")
	private ModelAndView toEdit(Long hospital_id) {
		try {

			//TODO 这里需要根据医院id获取医院对象放到map里

			Map<String, Object> map = new HashMap<String, Object>();
//

			//TODO 同时还需要获取项目列表放到map里，用于配置项目列表
			ModelAndView modelAndView = getTemplate("system/project/save", map);

			return modelAndView;
		} catch (Exception e) {
			logger.error("修改医院页面初始化异常", e);
			ModelAndView modelAndView = getTemplate("uncaughtException", null);
			return modelAndView;
		}
	}

	@RequestMapping(value = "toCheck")
	private ModelAndView toCheck(Long hospital_id) {
		try {
			//TODO 这里需要根据医院id获取医院对象放到map里
			Map<String, Object> map = new HashMap<String, Object>();
			//TODO 获取所有部位
			JSONArray partArray = orderService.getPartAll();
			map.put("partArray", partArray);
			//TODO 根据部位ID获取项目列表
			JSONArray projectArray = hospitalService.getProjectListByPart("1");
			map.put("projectArray", projectArray);
			//TODO 同时还需要获取项目列表放到map里，用于配置项目列表
			ModelAndView modelAndView = getTemplate("hospital/check", map);

			return modelAndView;
		} catch (Exception e) {
			logger.error("修改医院页面初始化异常", e);
			ModelAndView modelAndView = getTemplate("uncaughtException", null);
			return modelAndView;
		}
	}

	@RequestMapping(value = "getHospital")
	@ResponseBody
	public ReturnObj getCityByParentId(@RequestParam String hospitalName, @RequestParam int state) {
		logger.info("/hospital/getHospitals?hospitalName=" + hospitalName + "&state=" + state);
		ReturnObj returnObj = null;
		//TODO 查看所有已经注册的医院信息列表
		JSONArray jsonArray = hospitalService.getHospitalAll(hospitalName, state);
		returnObj = ReturnObj.createSuccess(jsonArray);
		return returnObj;
	}

	;

	@RequestMapping(value = "getPorjectListByPart")
	@ResponseBody
	public ReturnObj getPorjectListByPart(@RequestParam String partId) {
		logger.info("/hospital/getPorjectListByPart?partId=" + partId);
		ReturnObj returnObj = null;
		//TODO 根据部位ID获取项目列表
		JSONArray jsonArray = hospitalService.getPorjectListByPart(partId);
		returnObj = ReturnObj.createSuccess(jsonArray);
		return returnObj;
	}

	;


	/**
	 * 添加医院的基础信息
	 *
	 * @param hospital
	 * @return
	 */
	@RequestMapping(value = "addHospital")
	public ReturnObj addHospital(@RequestBody String hospital) {
		logger.info("/hospital/addHospital?hospital=" + hospital);
		ReturnObj returnObj = null;
		//TODO 保存医院信息
		JSONObject jsonObject = hospitalService.addHospital(hospital);
		returnObj = ReturnObj.createSuccess(jsonObject);
		return returnObj;
	}

	@RequestMapping(value = "unApproveHospitalList")
	@ResponseBody
	public ReturnObj unApproveHospitalList(@RequestParam String hospitalName, @RequestParam int state) {
		logger.info("/hospital/unApproveHospitalList?hospitalName=" + hospitalName + "state=" + state);
		ReturnObj returnObj = null;
		//TODO  查看所有已经注册但未审核的医院信息列表
		JSONArray jsonArray = hospitalService.getUnApproveHospitalListByName(hospitalName, state);
		returnObj = ReturnObj.createSuccess(jsonArray);
		return returnObj;
	}

	@RequestMapping(value = "saveHospital")
	@ResponseBody
	public ReturnObj saveHospital(Object hospital) {
		ReturnObj returnObj = null;
		//TODO 保存医院认证信息
		JSONObject jsonObject = hospitalService.saveHospital(hospital);
		returnObj = ReturnObj.createSuccess(jsonObject);
		return returnObj;
	}



	/**
	 * 医院列表
	 *
	 * @return
	 */
	@RequestMapping(value = "/rolelist")
	@ResponseBody
	public ReturnObj list(@Valid Page page) {
		ReturnObj returnObj = new ReturnObj();
		try {
			return ReturnObj.createSuccess(hospitalService.getResourcePage(page));
		} catch (Exception e) {
			e.getCause();
			ResponseJsonUtil.paramContentError(returnObj, "系统繁忙");
		}
		return returnObj;
	}
}
