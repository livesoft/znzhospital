package com.znz.web;

import com.znz.util.common.ReturnObj;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by IntelliJ IDEA.
 *
 * 此为创建接口的例子，和初期需求做的功能
 * User: johnson
 * Date: 4/25/15
 * Time: 11:47 上午
 */
@RequestMapping("/example")
@Controller
public class Example {


	private static Logger logger = LoggerFactory.getLogger(Example.class);

	/**
	 * 获取请求的是参数接口
	 * @return
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public ReturnObj getExample(@RequestParam String id ) {
		ReturnObj returnObj = null;

		/**
		 * 请求的参数用来复现错误请求的参数
		 */
		logger.info("/example/list?id="+id);

		JSONObject jsonObject = new JSONObject();
		JSONObject jsonObject1=new JSONObject();
        jsonObject1.put("name","周杰伦");
        jsonObject1.put("id","14");
        jsonObject.put("name", "刘德华");
		jsonObject.put("id", "12");
		JSONArray jsonArray = new JSONArray();
		jsonArray.add(jsonObject);

		if (id.equals("1")) {
			returnObj = ReturnObj.createSuccess(jsonArray);
		}    else if(id.equals("2")) {
			returnObj = ReturnObj.createError(jsonArray);
		}
		return returnObj;
	}
}
