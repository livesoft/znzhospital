package com.znz.web;

import com.znz.service.CustomerSerService;
import com.znz.util.common.ReturnObj;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * User: duyuxin
 * Date: 15-4-30
 * Time: 下午8:55
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/CustomerSer")
public class CustomerSerController {
    @Autowired
    private CustomerSerService customerSerService;

    private Logger logger = LoggerFactory.getLogger(CustomerSerController.class);

    @RequestMapping(value = "getCustomerServiceList")
    @ResponseBody
    public ReturnObj getCustomerServiceList(@RequestParam String userName,@RequestParam String userAlias,@RequestParam String userPhone,@RequestParam int userLevel,@RequestParam int state){
         ReturnObj returnObj=null;
         //TODO 查看所有已经注册的客服账户列表
         logger.info("/CustomerSer/getCustomerServiceList?userName="+userName+"&userAlias="+userAlias+"&userPhone="+userPhone+"&userLevel="+userLevel+"&state="+state);
         JSONArray jsonArray=customerSerService.getCustomerServiceList(userName,userAlias,userPhone,userLevel,state);
         returnObj=ReturnObj.createSuccess(jsonArray);
         return  returnObj;
    }

    @RequestMapping(value = "getProjectListByPartId")
    @ResponseBody
    public ReturnObj getProjectListByPartId(@RequestParam String partId){
        ReturnObj returnObj=null;
        //TODO 根据部位查询项目
        logger.info("/CustomerSer/getProjectListByPartId?partId="+partId);
        JSONArray jsonArray=customerSerService.getProjectListByPartId(partId);
        returnObj=ReturnObj.createSuccess(jsonArray);
        return returnObj;
    }

    @RequestMapping(value = "saveCustomerService")
    @ResponseBody
    public ReturnObj saveCustomerService(Object customerServiceVo){
         ReturnObj returnObj=null;
        //TODO 保存客服信息
        logger.info("");
        JSONObject jsonObject=customerSerService.saveCustomerService(customerServiceVo);
        returnObj=ReturnObj.createSuccess(jsonObject);
        return  returnObj;
    }
}
