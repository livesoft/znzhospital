package com.znz.service;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: duyuxin
 * Date: 15-4-28
 * Time: 下午9:09
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CustomerSerService {

    //TODO  查看所有已经注册的客服账户列表
    /**
     * @param userName  账户名称
     * @param userAlias 客服别名
     * @param userPhone 联系电话
     * @param userLevel 客服级别
     * @param state     状态
     * @return
     */
    public JSONArray getCustomerServiceList(String userName,String userAlias,String userPhone,int userLevel,int state){
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("id",1);
        jsonObject.put("userName","客服1");
        jsonObject.put("userAlias","小美");
        jsonObject.put("userPhone","130123402311");
        jsonObject.put("percentage",3);
        jsonObject.put("userLevel",2);
        jsonObject.put("state",1);
        JSONArray jsonArrayBe=new JSONArray();
        JSONObject jsonObject1Be1=new JSONObject();
        JSONObject jsonObject1Be2=new JSONObject();
        jsonObject1Be1.put("id",1);
        jsonObject1Be1.put("projectName","隆鼻");
        jsonObject1Be2.put("id",2);
        jsonObject1Be2.put("projectName", "双眼皮");
        jsonArrayBe.add(jsonObject1Be1);
        jsonArrayBe.add(jsonObject1Be2);
        jsonObject.put("beGoodAtProjectList",jsonArrayBe);
        jsonArray.add(jsonObject);
        return  jsonArray;
    }
     //TODO 根据部位查询项目
    /**
     * @param partId 部位ID
     * @return
     */
    public JSONArray getProjectListByPartId(String partId){
         JSONArray jsonArray=new JSONArray();
         JSONObject jsonObj1=new JSONObject();
         jsonObj1.put("id",1);
         jsonObj1.put("projectName","项目名称");
        jsonArray.add(jsonObj1);
        return jsonArray;
    }
    //TODO 保存客服信息
    /**
     * @param customerServiceVo 客服对象
     * @return
     */
    public JSONObject saveCustomerService(Object customerServiceVo){
         return new JSONObject();
    }

}
