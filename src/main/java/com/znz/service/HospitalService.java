package com.znz.service;

import com.znz.domain.Thospital;
import com.znz.domain.ThospitalProjectAgreement;
import com.znz.util.common.Page;
import com.znz.util.common.Tools;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: johnson
 * Date: 4/26/15
 * Time: 2:01 下午
 */

@Service
public class HospitalService {
	//TODO 查看所有已经注册的医院信息列表       创建医院账户

	/**
	 * @param hospitalName 医院名称
	 * @param state        医院状态
	 * @return
	 */
	public JSONArray getHospitalAll(String hospitalName, int state) {
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObj1 = new JSONObject();
		JSONObject jsonObj1Pro = new JSONObject();
		JSONObject jsonObj1City = new JSONObject();
		JSONObject jsonObj1Hosp = new JSONObject();
		JSONObject jsonObj1Level = new JSONObject();
		jsonObj1.put("id", 1);
		jsonObj1.put("hospitalName", "北京东方瑞丽");
		jsonObj1.put("hospitalAlias", "东方瑞丽");
		jsonObj1.put("hospitalPhone", "1314243134112");
		jsonObj1.put("address", "苏州街公寓");
		jsonObj1Pro.put("id", 1);
		jsonObj1Pro.put("name", "北京");
		jsonObj1City.put("id", 212);
		jsonObj1City.put("name", "海淀");
		jsonObj1Hosp.put("id", 302);
		jsonObj1Hosp.put("name", "私立医院");
		jsonObj1Level.put("id", 402);
		jsonObj1Level.put("name", "三级");
		jsonObj1.put("province", jsonObj1Pro);
		jsonObj1.put("city", jsonObj1City);
		jsonObj1.put("hospitalProperty", jsonObj1Hosp);
		jsonObj1.put("projectLevel", jsonObj1Level);
		jsonObj1.put("featureProject", "耳软骨鼻部塑形");
		jsonObj1.put("contractLimitDate", "2015-09-09");
		jsonObj1.put("isAuthentication", 1);
		jsonObj1.put("state", 4);
		jsonArray.add(jsonObj1);
		return jsonArray;
	}

	;
	//TODO  根据部位查询项目   创建医院账户

	/**
	 * @param partId 部位ID
	 * @return JSONArray
	 */
	public JSONArray getPorjectListByPart(String partId) {
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("id", 1);
		jsonObj1.put("projectName", "项目名称");
		jsonArray.add(jsonObj1);
		return jsonArray;
	}

	;
	//TODO 保存医院信息    创建医院账户

	/**
	 * @param hospital 医院对象
	 * @return JSON
	 */
	public JSONObject addHospital(String hospital) {
		//保存医院的基础信息
		JSONObject hospitalDetail = JSONObject.fromObject(hospital);//医院基础信息
		Thospital thospital = getThospital(hospitalDetail);
		int indexnum = 0;
		while (getObjectjson("projectName", hospitalDetail, indexnum) != null) {
			indexnum = indexnum + 1;
		}
//		保存医院和合约项目之间的关系
		for (int i = 0; i < indexnum; i++) {
			saveThospitalProjectAgreeement(hospitalDetail, thospital, i);
		}

		return new JSONObject();
	}

	/**
	 * 保存医院信息
	 * @param hospitalDetail
	 * @return
	 */
	private Thospital getThospital(JSONObject hospitalDetail) {
		Thospital thospital = new Thospital();
		thospital.setHospitalName(hospitalDetail.getString("hospitalName"));
//		thospital.setCertification(hospitalDetail.getString("hospitalAddress"));
		thospital.setCity(hospitalDetail.getString("city"));
//		thospital.setContract_deadline(hospitalDetail.getString("contractDeadline"));
		thospital.setCreate_time(new Date());
		thospital.setFeaturesProject(hospitalDetail.getString("featuresProject"));
		thospital.setHospitalAddress(hospitalDetail.getString("hospitalAddress"));
		thospital.setHospitalAlias(hospitalDetail.getString("hospitalAlias"));
		thospital.setHospitalNature(hospitalDetail.getString("hospitalNature"));
		thospital.setHospitalUrl(hospitalDetail.getString("hospitalUrl"));
		thospital.setPhone(hospitalDetail.getString("phone"));
		thospital.setProjectLevel(hospitalDetail.getString("projectLevel"));
		thospital.setProvince(hospitalDetail.getString("province"));
		thospital.setStatus("1");
		thospital.setVip_flag("0");
		thospital.setUpdate_time(new Date());
//		联系人的添加
		JSONArray contactList = getContactJsonArray(hospitalDetail);
		thospital.setContacts(contactList.toString());
		thospital.persist();
		return thospital;
	}

	/**
	 * 保存医院与合约项目的之间的关系
	 *
	 * @param hospitalDetail
	 * @param thospital
	 * @param i
	 */
	private void saveThospitalProjectAgreeement(JSONObject hospitalDetail, Thospital thospital, int i) {
		ThospitalProjectAgreement thospitalProjectAgreement = new ThospitalProjectAgreement();
		thospitalProjectAgreement.setHospitalId(thospital.getId());
		thospitalProjectAgreement.setProjectName(getObjectjson("projectName", hospitalDetail, i).toString());
		thospitalProjectAgreement.setCommission(getObjectjson("commission", hospitalDetail, i).toString());
		thospitalProjectAgreement.setProjectID(Long.parseLong(getObjectjson("projectID", hospitalDetail, i).toString()));
		thospitalProjectAgreement.setIndicativePrice(Tools.getBigDecimalByString(getObjectjson("indicativePrice", hospitalDetail, i).toString()));
		thospitalProjectAgreement.persist();
	}


	/**
	 * 获取联系人拼接json数据
	 *
	 * @param hospitalDetail
	 * @return
	 */
	private JSONArray getContactJsonArray(JSONObject hospitalDetail) {
		JSONArray contactList = new JSONArray();
		JSONObject contact = new JSONObject();
		contact.put("contactsName", hospitalDetail.getString("contactsName"));
		contact.put("gender", hospitalDetail.getString("gender"));
		contact.put("contactsPhone", hospitalDetail.getString("contactsPhone"));
		contact.put("position", hospitalDetail.getString("position"));
		contact.put("email", hospitalDetail.getString("email"));
		contact.put("qq", hospitalDetail.getString("qq"));
		contact.put("wechat", hospitalDetail.getString("wechat"));

		contactList.add(contact);
		return contactList;
	}

	private Object getObjectjson(String key, JSONObject hospitalDetail, int indexnum) {
		return hospitalDetail.get(key + "[" + indexnum + "]");
	}


	//TODO 查看所有已经注册但未审核的医院信息列表 医院审核

	/**
	 * @param hospitalName 医院名称
	 * @param state        医院状态
	 * @return
	 */
	public JSONArray getUnApproveHospitalListByName(String hospitalName, int state) {
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObj1 = new JSONObject();
		JSONObject jsonObj1Pro = new JSONObject();
		JSONObject jsonObj1City = new JSONObject();
		JSONObject jsonObj1Hosp = new JSONObject();
		JSONObject jsonObj1Level = new JSONObject();
		jsonObj1.put("id", 1);
		jsonObj1.put("hospitalName", "北京东方瑞丽");
		jsonObj1.put("hospitalAlias", "东方瑞丽");
		jsonObj1.put("hospitalPhone", "1314243134112");
		jsonObj1.put("address", "苏州街公寓");
		jsonObj1Pro.put("id", 1);
		jsonObj1Pro.put("name", "北京");
		jsonObj1City.put("id", 212);
		jsonObj1City.put("name", "海淀");
		jsonObj1Hosp.put("id", 302);
		jsonObj1Hosp.put("name", "私立医院");
		jsonObj1Level.put("id", 402);
		jsonObj1Level.put("name", "三级");
		jsonObj1.put("province", jsonObj1Pro);
		jsonObj1.put("city", jsonObj1City);
		jsonObj1.put("hospitalProperty", jsonObj1Hosp);
		jsonObj1.put("projectLevel", jsonObj1Level);
		jsonObj1.put("featureProject", "耳软骨鼻部塑形");
		jsonObj1.put("contractLimitDate", "2015-09-09");
		jsonObj1.put("isAuthentication", 1);
		jsonObj1.put("state", 4);
		jsonArray.add(jsonObj1);
		return jsonArray;
	}

	;
	//TODO 根据部位查询项目     医院审核

	/**
	 * @param partId 部位ID
	 * @return
	 */
	public JSONArray getProjectListByPart(String partId) {
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("id", 1);
		jsonObj1.put("projectName", "项目名称");
		jsonArray.add(jsonObj1);
		return jsonArray;
	}

	//TODO　保存医院认证信息    医院审核

	/**
	 * @param hospital 医院对象
	 * @return
	 */
	public JSONObject saveHospital(Object hospital) {
		return new JSONObject();
	}



	/**
	 *分页获取医院列表
	 * @param page
	 * @return
	 * @throws Exception
	 */
	public Page<Thospital> getResourcePage(Page<Thospital> page) throws  Exception{
		page.setTotal((int) Thospital.countThospitals());
		page.setRows(
				Thospital.findThospitalEntries(
						page.getOffset(),  //
						page.getOffset() + page.getLimit(), page.getSort(), page.getOrder()
				)
		);
		return page;
	}

}
