package com.znz.service;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: duyuxin
 * Date: 15-4-27
 * Time: 下午10:06
 * To change this template use File | Settings | File Templates.
 */
@Service
public class OrderService {
    //TODO 根据上级省市ID获取下级所有省市级数据  订单创建页面
    /**
     * @param  parentId 上级省市ID
     * @return  JSONArray
     */
    public JSONArray getProvinceAllByParengId(int parentId){
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject1=new JSONObject();
        jsonObject1.put("id",1);
        jsonObject1.put("name","北京市");
        JSONObject jsonObject2=new JSONObject();
        jsonObject2.put("id",2);
        jsonObject2.put("name","河南省");
        JSONObject jsonObject3=new JSONObject();
        jsonObject3.put("id",3);
        jsonObject3.put("name","山东省");
        JSONObject jsonObject4=new JSONObject();
        jsonObject4.put("id",4);
        jsonArray.add(jsonObject1);
        jsonArray.add(jsonObject2);
        jsonArray.add(jsonObject3);
        jsonArray.add(jsonObject4);
        return jsonArray;
    };
    //TODO 获取所有的部位
    /**
     * @return   JSONArray
     */
    public JSONArray getPartAll(){
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject1=new JSONObject();
        jsonObject1.put("id",1);
        jsonObject1.put("name","鼻子");
        JSONObject jsonObject2=new JSONObject();
        jsonObject2.put("id",2);
        jsonObject2.put("name","眼睛");
        JSONObject jsonObject3=new JSONObject();
        jsonObject3.put("id",3);
        jsonObject3.put("name","脸");
        JSONObject jsonObject4=new JSONObject();
        jsonObject4.put("id",4);
        jsonObject4.put("name","大腿");
        jsonArray.add(jsonObject1);
        jsonArray.add(jsonObject2);
        jsonArray.add(jsonObject3);
        jsonArray.add(jsonObject4);
        return   jsonArray;
    };
    //TODO 根据部位获取该部位所包含的项目接口
    /**
     * @param partId 部位ID
     * @return JSONArray
     */
    public JSONArray getProjectByPartId(int partId){
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonobj1=new JSONObject();
        jsonobj1.put("id",1);
        jsonobj1.put("name","鼻子抽脂");
        JSONObject jsonobj2=new JSONObject();
        jsonobj2.put("id",2);
        jsonobj2.put("name","鼻梁垫高");
        JSONObject jsonobj3=new JSONObject();
        jsonobj3.put("id",3);
        jsonobj3.put("name","鼻头填充");
        JSONObject jsonobj4=new JSONObject();
        jsonobj4.put("id",4);
        jsonobj4.put("name","修剪鼻翼");
        jsonArray.add(jsonobj1);
        jsonArray.add(jsonobj2);
        jsonArray.add(jsonobj3);
        jsonArray.add(jsonobj4);
        return   jsonArray;
    }
    //TODO 保存订单
    public JSONObject saveOrder(){
        JSONObject jsonObject=new JSONObject();
        return   jsonObject;
    }


    //TODO  根据省份代码或者根据市区代码获取其下所有的医院  订单派发页面

    /**
     * @param cityId       城市ID
     * @param province     省份ID
     * @param hospitalName 医院名称
     * @return
     */
    public JSONArray getHospitalAllByCityId(int cityId,int province,String hospitalName){
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject1=new JSONObject();
        jsonObject1.put("id",1);
        jsonObject1.put("name","北京中日友好医院");
        //TODO 特色项目
        JSONArray jsonObject1Arr=new JSONArray();
        JSONObject jsonObjectArr1=new JSONObject();
        jsonObjectArr1.put("id",1);
        jsonObjectArr1.put("projectName","抽脂");
        JSONObject jsonObjectArr2=new JSONObject();
        jsonObjectArr2.put("id",2);
        jsonObjectArr2.put("projectName","双眼皮");
        jsonObject1Arr.add(jsonObjectArr1);
        jsonObject1Arr.add(jsonObjectArr2);
        jsonObject1.put("featureProject",jsonObject1Arr);
        jsonObject1.put("tel","12312131231");

        JSONObject jsonObject2=new JSONObject();
        jsonObject2.put("id",1);
        jsonObject2.put("name","北京协和医院");
        jsonObject2.put("tel","12312131231");
        jsonArray.add(jsonObject1);
        jsonArray.add(jsonObject2);
        return jsonArray;
    }
    //TODO 根据医院名称对医院进行模糊查询接口
    /**
     * @param hospitalName 医院名称
     * @return
     */
    public JSONArray getHospitalByLikeName(String hospitalName){
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject1=new JSONObject();
        jsonObject1.put("id",1);
        jsonObject1.put("name","北京中日友好医院");
        JSONObject jsonObject2=new JSONObject();
        jsonObject2.put("id",1);
        jsonObject2.put("name","北京中日协和医院");
        jsonArray.add(jsonObject1);
        jsonArray.add(jsonObject2);
        return jsonArray;
    }
    //TODO 获取订单分配所有的医院接口
    /**
     * @param  orderId  订单ID
     * @return
     */
    public JSONArray getHospitalAllByOrderId(int orderId){
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObjet1=new JSONObject();
        jsonObjet1.put("id",1);
        jsonObjet1.put("name","北京中日友好医院");
        jsonObjet1.put("state",0);
        JSONObject jsonObjet2=new JSONObject();
        jsonObjet2.put("id",2);
        jsonObjet2.put("name","北京第三医院");
        jsonObjet2.put("state",1);
        jsonArray.add(jsonObjet1);
        jsonArray.add(jsonObjet2);
        return jsonArray;
    }
    //TODO 保存订单分配的医院
    /**
     * @param orderId  订单iD
     * @param hospitalProjectId  医院项目ID
     * @param expectCostMin    期望花费最少钱
     * @param expectCostMax    期望花费最多钱
     * @return
     */
    public JSONObject saveHospitalProject(int orderId, int hospitalProjectId,int expectCostMin,int expectCostMax){
        JSONObject jsonObject=new JSONObject();
        return jsonObject;
    }
}
