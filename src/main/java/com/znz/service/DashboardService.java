package com.znz.service;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * User: duyuxin
 * Date: 15-4-28
 * Time: 下午9:59
 * To change this template use File | Settings | File Templates.
 */
@Service
public class DashboardService {

    //TODO 查看当前客服的所有订单信息，如果是管理员则查看所有订单信息
    /**
     * @param userPhone 用户联系方式
     * @param userId   用户ID
     * @return
     */
    public JSONArray getAllOrderList(String userPhone,String userId){
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject1=new JSONObject();
        jsonObject1.put("id",1);
        jsonObject1.put("userName","王美丽");
        jsonObject1.put("state",4);
        JSONArray jsonArrayOrder=new JSONArray();
        JSONObject  jsonObjectOrder1=new JSONObject();
        JSONObject  jsonObjectOrder2=new JSONObject();
        jsonObjectOrder1.put("id",1);
        jsonObjectOrder1.put("projectName","隆鼻");
        jsonObjectOrder1.put("cost",18000);
        jsonObjectOrder2.put("id",2);
        jsonObjectOrder2.put("projectName","双眼皮");
        jsonObjectOrder2.put("cost", 21000);
        jsonArrayOrder.add(jsonObjectOrder1);
        jsonArrayOrder.add(jsonObjectOrder2);
        jsonObject1.put("orderHospitalProjectList",jsonArrayOrder);
        jsonArray.add(jsonObject1);
        return jsonArray;
    }
    //TODO 根据当前客服id获取当月（自然月）业绩总额，如果是超级管理员则全部客服的业绩总额
    /**
     * @param userId 用户ID
     * @return
     */
    public JSONObject getAchievement(String userId){
          JSONObject jsonObject=new JSONObject();
          jsonObject.put("achievement",2020000);
        return jsonObject;
    }
    //TODO 根据当前客服id获取分配给自己的美丽需求数量
    /**
     * @param userId 用户ID
     * @return
     */
    public JSONObject getDesireCount(String userId){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("desire",10);
        return jsonObject;
    }
    //TODO 根据当前客服id获取当天待回访的数量
    /**
     * @param userId  用户ID
     * @return
     */
    public JSONObject getReturnVisitCount(String userId){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("returnVisit",10);
        return  jsonObject;
    }
}
