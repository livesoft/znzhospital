package com.znz.util.common;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.zip.Deflater;

/**
 * 工具类
 * 
 * @author Administrator
 * 
 */
public class Tools {

	private static Logger logger = LoggerFactory.getLogger(Tools.class);

	/**
	 * 验证参数是否为空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String string) {
		if (StringUtils.isEmpty(string)) {
			return true;
		}
		if (string.trim().equals("")) {
			return true;
		}
		return false;
	}



	/**
	 * MD5加密（内部系统使用）
	 * 
	 * @param str
	 * @return
	 */
	public static String EncoderByMd5(String str) {
		String newstr = ""; // 加密后的字符串
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			BASE64Encoder base64en = new BASE64Encoder();
			newstr = base64en.encode(md5.digest(str.getBytes("utf-8")));
		} catch (Exception e) {
			logger.error("error", e);
		}
		return newstr;
	}

	/**
	 * MD5加密（外部合作使用）
	 * 
	 * @param str
	 * @return
	 */
	public static String md5(String str) {
		String result = "";
		try {
			byte[] btInput = str.getBytes();
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < md.length; i++) {
				int val = ((int) md[i]) & 0xff;
				if (val < 16) {
					sb.append("0");
				}
				sb.append(Integer.toHexString(val));
			}
			result = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			logger.error("error", e);
		}
		return result;
	}

	/**
	 * 生成随机数
	 * 
	 * @param num
	 *            位数
	 */
	public static String generateRandomNum(int num) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < num; i++) {
			Random random = new Random();
			int nextInt = random.nextInt(10);
			builder.append(nextInt);
		}
		return builder.toString();
	}

	/**
	 * 替换特殊符号
	 * 
	 * @param string
	 * @return
	 */
	public static String replaceSpecialCharacter(String string) {
		if (string != null) {
			string = string.trim().replaceAll(",", "").replaceAll("\n", "")
					.replaceAll(">", "");
		}
		return string;
	}

	/**
	 * 压缩数据
	 * 
	 * @param data
	 * @return byte[]
	 */
	public static byte[] compress(byte[] data) {
		byte[] output = new byte[0];
		Deflater compresser = new Deflater();
		compresser.reset();
		compresser.setInput(data);
		compresser.finish();
		ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
		try {
			byte[] buf = new byte[1024];
			while (!compresser.finished()) {
				int i = compresser.deflate(buf);
				bos.write(buf, 0, i);
			}
			output = bos.toByteArray();
		} catch (Exception e) {
			output = data;
			logger.error("error", e);
		} finally {
			try {
				bos.close();
			} catch (IOException e) {
				logger.error("error", e);
			}
		}
		compresser.end();
		return output;
	}

	/**
	 * base64编码
	 * 
	 * @param src
	 * @param charset
	 * @return
	 */
	public static String base64Encode(byte[] src, String charset) {
		Base64 base64 = new Base64();
		try {
			return new String(base64.encode(src), charset);
		} catch (UnsupportedEncodingException e) {
			logger.error("error", e);
			return null;
		}
	}

	/**
	 * base64解码
	 * 
	 * @param src
	 * @param charset
	 * @return
	 */
	public static byte[] base64Decode(String src, String charset) {
		Base64 base64 = new Base64();
		try {
			return base64.decode(src.getBytes(charset));
		} catch (UnsupportedEncodingException e) {
			logger.error("error", e);
			return null;
		}
	}
	
	/**
	 * 按key排序Map
	 * 
	 * @param map
	 * @param reverse
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map sortMapByKey(Map map, final boolean reverse) {
		List list = new LinkedList(map.entrySet());
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				if (reverse) {
					return -((Comparable) ((Map.Entry) (o1)).getKey())
							.compareTo(((Map.Entry) (o2)).getKey());
				}
				return ((Comparable) ((Map.Entry) (o1)).getKey())
						.compareTo(((Map.Entry) (o2)).getKey());
			}
		});

		Map result = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	/**
	 * 按value排序Map
	 * 
	 * @param map
	 * @param reverse
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map sortMapByValue(Map map, final boolean reverse) {
		List list = new LinkedList(map.entrySet());
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				if (reverse) {
					return -((Comparable) ((Map.Entry) (o1)).getValue())
							.compareTo(((Map.Entry) (o2)).getValue());
				}
				return ((Comparable) ((Map.Entry) (o1)).getValue())
						.compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		Map result = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	/**
	 * 将数据格式成两位小数
	 * 
	 * @param data
	 * @return
	 */
	public static String formatTwoDecimal(String data) {
		if (StringUtils.isBlank(data)||StringUtils.equals(data, "null")) {
			return "";
		}
		DecimalFormat df = new DecimalFormat("0.00");
 		return df.format(Double.parseDouble(data));
	}
	
	/**
	 * 是否是偶数
	 * @param num
	 * @return
	 */
	public static boolean isEvenNumber(int num) {
		if (num%2==0) { //是偶数
			return true;
		}
		return false;
	}
	
	/**
	 * 是否是质数
	 * @param num
	 * @return
	 */
	public static boolean isPrimeNumber(int num) {
		for(int i=2; i<num; i++) {
			if(num%i==0){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 得到起始索引
	 * @param pageIndex
	 * @param maxResult
	 * @return
	 */
	public static Integer getStartIndex(String pageIndex, String maxResult) {
		return Integer.valueOf(pageIndex)*Integer.valueOf(maxResult);
	}
	
	/**
	 * 得到结束索引
	 * @param pageIndex
	 * @param maxResult
	 * @return
	 */
	public static Integer getEndIndex(String pageIndex, String maxResult) {
		return Integer.valueOf(pageIndex)*Integer.valueOf(maxResult) + Integer.valueOf(maxResult);
	}
	
	/**
	 * 得到总页数
	 * @param totalResult
	 * @param maxResult
	 * @return
	 */
	public static Integer getTotalPage(Integer totalResult, Integer maxResult) {
		return (totalResult%maxResult==0) ? (totalResult/maxResult) : (totalResult/maxResult+1);
	}
	
	/*public static void main(String[] args) {
		String str = "15";
		System.out.println(str.substring(str.length()-1, str.length()));
	}*/
	
	/**
	 * 字符串是否是数字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
	    Pattern pattern = Pattern.compile("[0-9]*");
	    return pattern.matcher(str).matches();
	}


	public static BigDecimal getBigDecimalByString(String str) {

		DecimalFormat decimalFormat = new DecimalFormat();
		decimalFormat.setParseBigDecimal(true);

		// parse the string
		BigDecimal bigDecimal = null;
		try {
			bigDecimal = (BigDecimal) decimalFormat.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
		return bigDecimal;
 	}


}