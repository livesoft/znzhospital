package com.znz.util.cache;

/**
 * Created by IntelliJ IDEA.
 * User: johnson
 * Date: 3/11/15
 * Time: 10:31 上午
 */
public interface CacheService {

	public abstract <T> void set(String key, T t);

	public abstract <T> void set(String key, Integer exp, T t);

	public abstract <T> T get(String key);

	public abstract <T> void checkToSet(String key, T t);

	public abstract void delete(String key);

	public abstract void flushAll();

}
