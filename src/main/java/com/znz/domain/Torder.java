package com.znz.domain;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: johnson
 * Date: 5/7/15
 * Time: 3:39 下午
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField="",identifierColumn = "id", identifierField = "id",table = "t_order")
public class Torder {
//	订单号,系统内唯一
	private String order_no;
//	客户姓名
	private String user_name;
//	客户年龄
	private String user_age;
//	客户电话
	private String user_phone;
//	用户所在城市
	private String user_city;
//	用户留言
	private String user_message;
//	客户性别
	private String user_sex;
//	订单备注
	private String note;
//	创建订单客服ID
	private Integer c_user_id;
//	状态1初始化2已派单3已接单4完单
	private String state;
//	接单回访状态0 未回访 1 已回访
	private String return_visit_receive;
//	结单回访状态 0 未回访 1 已回访
	private String return_visit_success;
//	是否可用,是否删除0可用1删除
	private String enabled;
//	创建时间
	private Date create_date;
//	修改时间
	private Date update_date;

}
