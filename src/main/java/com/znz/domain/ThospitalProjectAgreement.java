package com.znz.domain;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.Column;
import java.math.BigDecimal;

/**
 *医院合约项目表
 * Created by IntelliJ IDEA.
 * User: johnson
 * Date: 5/9/15
 * Time: 7:34 下午
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField = "", identifierColumn = "id", identifierField = "id", table = "t_hospital_project_agreement")
public class ThospitalProjectAgreement {


	//	项目名称
	private String projectName;
	//	项目id
	private Long projectID;
	//	项目价格
	private BigDecimal indicativePrice;
	//医院id
	@Column(name = "hospital_id")
	private Long hospitalId;

	private String commission;
}
