package com.znz.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 字典表
 * Created with IntelliJ IDEA.
 * User: zhangshouchen
 * Date: 15-3-22
 * Time: 下午5:55
 * To change this template use File | Settings | File Templates.
 */
@RooToString
@RooJpaActiveRecord(versionField="",table = "t_dictionary")
public class Dictionary implements Serializable{

    /**
     *字典名称
     */
    private String dictionaryName;
    /**
     *字典编码
     */
    private String dictionaryCode;
    /**
     *可用状态
     */
    private int status;
    /**
     *字典描述
     */
    private  String describes;
    private  Date createDate;
    private  Date updateDate;
    /**
     *操作人
     */
    private  String operator;
    /**
     *父级字典
     */
    private  int parentId;
/*
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "dictionary",cascade= CascadeType.PERSIST)
    private Set<DictionaryDetail> details ;*/


    public String getDictionaryName() {
        return dictionaryName;
    }

    public void setDictionaryName(String dictionaryName) {
        this.dictionaryName = dictionaryName;
    }

    public String getDictionaryCode() {
        return dictionaryCode;
    }

    public void setDictionaryCode(String dictionaryCode) {
        this.dictionaryCode = dictionaryCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescribes() {
        return describes;
    }

    public void setDescribes(String describes) {
        this.describes = describes;
    }
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    /**
     * 根据参数查询字典信息，参数的key对应的是字典实体的属性名，不是数据库的字段名
     * @param params
     * @return
     * @throws Exception
     */
    public static List<Dictionary> findDictionariesList(Map<String,Object> params) throws Exception{
        StringBuilder str_query =  new StringBuilder("SELECT o FROM Dictionary o where 1=1 ");
        if(params!=null){
            Set<String> keySet = params.keySet();
            for(String key :keySet){
                str_query.append(" and o."+key+" = ?");
            }
            str_query.append(" order by o.id ");
            TypedQuery<Dictionary> q = entityManager().createQuery(
                    str_query.toString(), Dictionary.class);
            int i=1;
            for(String key :keySet){
                q.setParameter(i,params.get(key));
                i++;
            }
            return q.getResultList();
        }
        return null;
    }
/*    @JsonIgnore
    public Set<DictionaryDetail> getDetails() {
        return details;
    }

    public void setDetails(Set<DictionaryDetail> details) {
        this.details = details;
    }*/
}
