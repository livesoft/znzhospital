package com.znz.domain;

import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 字典明细表
 * Created with IntelliJ IDEA.
 * User: zhangshouchen
 * Date: 15-3-22
 * Time: 下午5:55
 * To change this template use File | Settings | File Templates.
 */
@RooToString
@RooJpaActiveRecord(versionField="",identifierColumn = "id", identifierField = "id", table = "t_dictionary_detail")
public class DictionaryDetail implements Serializable {

    private String dtext;
    private String dvalue;
    @ManyToOne
    @JoinColumn(name = "dictionary_id")
    private Dictionary dictionary;

    public String getDtext() {
        return dtext;
    }

    public void setDtext(String dtext) {
        this.dtext = dtext;
    }

    public String getDvalue() {
        return dvalue;
    }

    public void setDvalue(String dvalue) {
        this.dvalue = dvalue;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    /**
     * 根据参数查询字典信息，参数的key对应的是字典实体的属性名，不是数据库的字段名
     * @param params
     * @return
     * @throws Exception
     */
    public static List<DictionaryDetail> findDictionaryDetailsList(Map<String,Object> params) throws Exception{
        StringBuilder str_query =  new StringBuilder("SELECT o FROM DictionaryDetail o where 1=1 ");
        if(params!=null){
            Set<String> keySet = params.keySet();
            for(String key :keySet){
                str_query.append(" and o."+key+" = ?");
            }
            str_query.append(" order by o.id ");
            TypedQuery<DictionaryDetail> q = entityManager().createQuery(
                    str_query.toString(), DictionaryDetail.class);
            int i=1;
            for(String key :keySet){
                q.setParameter(i,params.get(key));
                i++;
            }
            return q.getResultList();
        }
        return null;
    }

}
