package com.znz.domain;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import java.util.Date;

/**
 * 客服与医院就订单的聊天记录
 * Created by IntelliJ IDEA.
 * User: johnson
 * Date: 5/7/15
 * Time: 3:47 下午
 */

@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField="",identifierColumn = "id", identifierField = "id",table = "t_order_hospital_chat")
public class Torderhospitalchat {


//	订单ID,对应t_order表ID
	private Integer order_id;
//	订单号
	private Integer order_no;
//	医院ID
	private Integer hospital_id;
//	医院用户ID
	private Integer h_user_id;

//	客服ID
	private Integer c_user_id;

//	用户名
	private String c_user_name;

//	聊天内容
	private String content;

//	创建日期
	private Date create_date;







}
