package com.znz.domain;

import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import java.io.Serializable;

/**
 * 整形部位
 * Created with IntelliJ IDEA.
 * User: zhangshouchen
 * Date: 15-3-30
 * Time: 上午11:07
 * To change this template use File | Settings | File Templates.
 */
@RooToString
@RooJpaActiveRecord(versionField="",identifierColumn = "id", identifierField = "id",table = "t_parts")
public class Part implements Serializable {

    private String partName;
    private String partIcon;
    private int orderNum;
    private int status;

    public String getPartName() {
        return partName;
    }


    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getPartIcon() {
        return partIcon;
    }

    public void setPartIcon(String partIcon) {
        this.partIcon = partIcon;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
