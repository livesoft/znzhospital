package com.znz.domain;

import com.znz.util.common.Page;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: johnson
 * Date: 5/6/15
 * Time: 2:28 下午
 */
@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField = "", identifierColumn = "id", identifierField = "id", table = "t_hospital")
public class Thospital {

	//医院名称
	@Column(name = "hospital_name")
	private String hospitalName;
	//	医院别名
	@Column(name = "hospital_alias")

	private String hospitalAlias;
	//	联系电话
	private String phone;
	//	省份
	private String province;
	//	城市
	private String city;
	//	医院地址
	@Column(name = "hospital_address")
	private String hospitalAddress;
	//	医院属性
	@Column(name = "hospital_nature")
	private String hospitalNature;
	//	项目级别
	@Column(name = "project_level")
	private String projectLevel;
	//	医院网址
	@Column(name = "hospital_url")
	private String hospitalUrl;
	//	特色项目
	@Column(name = "features_project")
	private String featuresProject;
	@Column(name = "contract_deadline")
	//	合同到期日期
	private Date contractDeadline;
	//状态 -1:删除 0:新建待审核 1:审核通过
	private String status;
	//	资质证明照片路径
	private String certification;
	//	0:不是vip 1:vip
	private String vip_flag;

	private Date create_time;
	private Date update_time;
	//联系人信息 json存储
	private String contacts;




}
