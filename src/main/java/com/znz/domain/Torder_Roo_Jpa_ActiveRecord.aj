// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.znz.domain;

import com.znz.domain.Torder;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Torder_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager Torder.entityManager;
    
    public static final List<String> Torder.fieldNames4OrderClauseFilter = java.util.Arrays.asList("order_no", "user_name", "user_age", "user_phone", "user_city", "user_message", "user_sex", "note", "c_user_id", "state", "return_visit_receive", "return_visit_success", "enabled", "create_date", "update_date");
    
    public static final EntityManager Torder.entityManager() {
        EntityManager em = new Torder().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Torder.countTorders() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Torder o", Long.class).getSingleResult();
    }
    
    public static List<Torder> Torder.findAllTorders() {
        return entityManager().createQuery("SELECT o FROM Torder o", Torder.class).getResultList();
    }
    
    public static List<Torder> Torder.findAllTorders(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Torder o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Torder.class).getResultList();
    }
    
    public static Torder Torder.findTorder(Long id) {
        if (id == null) return null;
        return entityManager().find(Torder.class, id);
    }
    
    public static List<Torder> Torder.findTorderEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Torder o", Torder.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<Torder> Torder.findTorderEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Torder o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Torder.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void Torder.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Torder.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Torder attached = Torder.findTorder(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Torder.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Torder.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Torder Torder.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Torder merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
