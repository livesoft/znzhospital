package com.znz.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(identifierColumn = "person_id", identifierField = "person_id", table = "person")
public class Person {

    /**
     */
    @NotNull
    private String name;
}
