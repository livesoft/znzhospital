// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.znz.domain;

import com.znz.domain.Tcustomer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tcustomer_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager Tcustomer.entityManager;
    
    public static final List<String> Tcustomer.fieldNames4OrderClauseFilter = java.util.Arrays.asList("userName", "password", "userAlias", "is_good_at", "userPhone", "percentage", "userLevel", "state", "update_time", "create_time", "hospital_id");
    
    public static final EntityManager Tcustomer.entityManager() {
        EntityManager em = new Tcustomer().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tcustomer.countTcustomers() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tcustomer o", Long.class).getSingleResult();
    }
    
    public static List<Tcustomer> Tcustomer.findAllTcustomers() {
        return entityManager().createQuery("SELECT o FROM Tcustomer o", Tcustomer.class).getResultList();
    }
    
    public static List<Tcustomer> Tcustomer.findAllTcustomers(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Tcustomer o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Tcustomer.class).getResultList();
    }
    
    public static Tcustomer Tcustomer.findTcustomer(Long id) {
        if (id == null) return null;
        return entityManager().find(Tcustomer.class, id);
    }
    
    public static List<Tcustomer> Tcustomer.findTcustomerEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tcustomer o", Tcustomer.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<Tcustomer> Tcustomer.findTcustomerEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Tcustomer o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Tcustomer.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void Tcustomer.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tcustomer.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tcustomer attached = Tcustomer.findTcustomer(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tcustomer.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tcustomer.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tcustomer Tcustomer.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tcustomer merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
