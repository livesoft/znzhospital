package com.znz.domain;

import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 整形项目
 * Created with IntelliJ IDEA.
 * User: zhangshouchen
 * Date: 15-3-30
 * Time: 上午11:06
 * To change this template use File | Settings | File Templates.
 */
@RooToString
@RooJpaActiveRecord(versionField="",identifierColumn = "id", identifierField = "id",table = "t_project")
public class Project {
    /**
     *项目名称
     */
    private String projectName;

    /**
     *icon
     */
    private String projectIcon;
    @ManyToOne
    @JoinColumn(name = "project_type")
    private DictionaryDetail projectType;
    /**
     *项目描述
     */
    private String description;
    /**
     *专业名称
     */
    private String professionalName;
    /**
     * 别名
     */
    private String alias;
    /**
     * 排序
     */
    private int orderNum;

    /**
     *项目部位
     */
    @ManyToOne
    @JoinColumn(name = "part_id")
    private Part part;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectIcon() {
        return projectIcon;
    }

    public void setProjectIcon(String projectIcon) {
        this.projectIcon = projectIcon;
    }

    public DictionaryDetail getProjectType() {
        return projectType;
    }

    public void setProjectType(DictionaryDetail projectType) {
        this.projectType = projectType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfessionalName() {
        return professionalName;
    }

    public void setProfessionalName(String professionalName) {
        this.professionalName = professionalName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }
}
