package com.znz.domain;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: johnson
 * Date: 5/7/15
 * Time: 4:48 下午
 */

@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField = "", identifierColumn = "id", identifierField = "id", table = "t_return_visit")
public class Treturnvisit {

	//订单ID
	private Integer order_id;
	//	订单号
	private Integer order_no;
	//	回访时间
	private Date return_visit_date;
	//	回访类型 0 普通回访 1 接单回访 2 结单回访
	private String type;
	//	客服ID
	private Integer c_user_id;
	//	回访前的备注
	private String remark;
	//	是否回访0否1是
	private String is_return_visit;
	//	回访后的记录
	private String return_visit_record;
	//	创建时间
	private String create_date;
	//	修改时间
	private String update_date;

}
