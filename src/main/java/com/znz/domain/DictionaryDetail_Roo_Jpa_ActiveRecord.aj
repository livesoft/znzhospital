// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.znz.domain;

import com.znz.domain.DictionaryDetail;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect DictionaryDetail_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager DictionaryDetail.entityManager;
    
    public static final List<String> DictionaryDetail.fieldNames4OrderClauseFilter = java.util.Arrays.asList("dtext", "dvalue", "dictionary");
    
    public static final EntityManager DictionaryDetail.entityManager() {
        EntityManager em = new DictionaryDetail().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long DictionaryDetail.countDictionaryDetails() {
        return entityManager().createQuery("SELECT COUNT(o) FROM DictionaryDetail o", Long.class).getSingleResult();
    }
    
    public static List<DictionaryDetail> DictionaryDetail.findAllDictionaryDetails() {
        return entityManager().createQuery("SELECT o FROM DictionaryDetail o", DictionaryDetail.class).getResultList();
    }
    
    public static List<DictionaryDetail> DictionaryDetail.findAllDictionaryDetails(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM DictionaryDetail o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, DictionaryDetail.class).getResultList();
    }
    
    public static DictionaryDetail DictionaryDetail.findDictionaryDetail(Long id) {
        if (id == null) return null;
        return entityManager().find(DictionaryDetail.class, id);
    }
    
    public static List<DictionaryDetail> DictionaryDetail.findDictionaryDetailEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM DictionaryDetail o", DictionaryDetail.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<DictionaryDetail> DictionaryDetail.findDictionaryDetailEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM DictionaryDetail o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, DictionaryDetail.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void DictionaryDetail.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void DictionaryDetail.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            DictionaryDetail attached = DictionaryDetail.findDictionaryDetail(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void DictionaryDetail.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void DictionaryDetail.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public DictionaryDetail DictionaryDetail.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        DictionaryDetail merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
