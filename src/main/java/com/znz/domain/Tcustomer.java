package com.znz.domain;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: johnson
 * Date: 5/6/15
 * Time: 3:11 下午
 */

@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField = "", identifierColumn = "id", identifierField = "id", table = "t_customer")

public class Tcustomer {

	//登录名
	private String userName;

	//登录密码
	private String password;
	//	客服别名
	private String userAlias;
	//擅长
	private String is_good_at;
	//	联系电话
	private String userPhone;
	//	提成比例
	private String percentage;
	//	客服等级
	private String userLevel;
	//	状态 删除-1 正常0 注销1
	private String state;
	//	修改时间
	private Date update_time;
	//	创建时间
	private Date create_time;

	//所属于医院的id
	private Integer hospital_id;

}
